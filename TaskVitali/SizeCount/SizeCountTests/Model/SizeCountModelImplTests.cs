﻿namespace SizeCount.Model.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.Linq;

    [TestClass()]
    public class SizeCountModelImplTests
    {
        [TestMethod()]
        public void AnswerTest()
        {
            ISizeCountModel Model = new SizeCountModelImpl();
            List<IIntegerOperation> Operations = Model.GetOperations();
            int Max = Operations.AsEnumerable().Select(e => e.GetResult()).Max();
            IIntegerOperation Op = null;
            int Count = 0;
            foreach (IIntegerOperation o in Operations)
            {
                if (o.GetResult() == Max)
                {
                    Op = o;
                    Count = Count + 1;
                }
            }

            if (Count == Operations.Count)
            {
                Assert.IsTrue(Model.IsCorrectAnswer("SAME"));
            }
            else
            {
                Assert.IsTrue(Model.IsCorrectAnswer(Op.GetOperationAsString()));
            }
        }  
    }
}