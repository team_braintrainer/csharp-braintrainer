﻿namespace SizeCount.Controller
{
    using System;

    /// <summary>
    /// A interface controller for the game size count.
    /// </summary>
    public interface ISizeCountController
    {      
        /// <summary>
        /// Informs the model of the new attempt of the user.
        /// </summary>
        /// <param name="answer">the submitted answer</param>
        void NewAttempt(string answer);
    }
}
