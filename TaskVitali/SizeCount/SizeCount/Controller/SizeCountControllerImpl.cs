﻿namespace SizeCount.Controller
{
    using SizeCount.Model;
    using SizeCount.View;

    /// <summary>
    /// The implementation of {@link SizeCountModel}.
    /// </summary>
    public class SizeCountControllerImpl : ISizeCountController
    {
        private readonly ISizeCountModel model;
        private readonly ISizeCountView view;

        public SizeCountControllerImpl(ISizeCountView view)
        {
            this.model = new SizeCountModelImpl();
            this.view = view;
            this.view.Draw(this.model.GetOperations());
        }

        public void NewAttempt(string answer)
        {
            if (this.model.IsCorrectAnswer(answer))
            {
                this.model.Reset();
                this.view.Draw(this.model.GetOperations());
            }
        }
    }
}
