﻿namespace SizeCount.Utility
{
    public class Pair<X, Y>
    {
        private readonly X first;
        private readonly Y second;

        public Pair(X first, Y second)
        {
            this.first = first;
            this.second = second;
        }

        public X GetFirst()
        {
            return this.first;
        }

        public Y GetSecond()
        {
            return this.second;
        }

        public override string ToString()
        {
            return "<" + first + "," + second + ">";
        }

    }
}
