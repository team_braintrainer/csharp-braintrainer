﻿namespace SizeCount
{
    using System;
    using System.Windows.Forms;
    using Prova.View;
    using SizeCount.Model;

    public class MainClass
    {
        [STAThread]
        public static void Main(string[] args)
        {
            IIntegerOperationsFactory factory = new IntegerOperationsFactoryImpl();

            IIntegerOperation op = factory.Addiction(2, 10);
            Console.WriteLine("Addizione: "+ op.GetOperationAsString() + " risultato: "+ op.GetResult().ToString());

            IIntegerOperation op1 = factory.Subtraction(2, 10);
            Console.WriteLine("Sottraizone: " + op1.GetOperationAsString() + " risultato: " + op1.GetResult().ToString());

            IIntegerOperation op2 = factory.Multiplication(2, 10);
            Console.WriteLine("Moltiplicaizone: " + op2.GetOperationAsString() + " risultato: " + op2.GetResult().ToString());

            IIntegerOperation op3 = factory.Division(2, 10);
            Console.WriteLine("Divisione: " + op3.GetOperationAsString() + " risultato: " + op3.GetResult().ToString());

            IIntegerOperation op4 = factory.SumExpression(2, 2, 10);
            Console.WriteLine("Espresisone somma: " + op4.GetOperationAsString() + " risultato: " + op4.GetResult().ToString());

            IIntegerOperation op5 = factory.SubExpression(2, 2, 10);
            Console.WriteLine("Espresisone somma: " + op5.GetOperationAsString() + " risultato: " + op5.GetResult().ToString());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SizeCountViewImpl());
        }
    }
}
