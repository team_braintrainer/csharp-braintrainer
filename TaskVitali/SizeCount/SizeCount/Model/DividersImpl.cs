﻿namespace SizeCount.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The implementation of the <see cref="IDividers"/>.
    /// </summary>
    public class DividersImpl : Model.IDividers
    {
        List<int> IDividers.GetDividers(int number)
        {
            List<int> dividers = new List<int>();
            double root = Math.Sqrt(number);
            for (int i = 1; i <= root; i++)
            {
                int divider = number % i;
                if (divider == 0)
                {
                    dividers.Add(i);
                    if (i != number / i)
                    {
                        dividers.Add(number / i);
                    }
                }
            }

            return dividers;
        }
    }
}
