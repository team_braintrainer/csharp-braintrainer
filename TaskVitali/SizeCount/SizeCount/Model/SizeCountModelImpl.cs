﻿namespace SizeCount.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SizeCount.Utility;

    /// <summary>
    /// The implementation of <see cref="ISizeCountModel"/>.
    /// </summary>
    public class SizeCountModelImpl : ISizeCountModel
    {

        private const int UPPER_BOUND = 50;

        private const int NUM_OPERATION_EXPR = 2;

        private const int NUM_OPERAND = 2;

        private const int NUM_OPERATIONS_GAME = 2;

        private const string SAME_ANSWER = "SAME";


        private readonly IIntegerOperationsFactory opFactory;
        private readonly List<Pair<IIntegerOperation, int>> operations;
        private readonly List<Operations> opOptions;
        private bool same;
        private IIntegerOperation maxOperation;

        public SizeCountModelImpl()
        {
            this.opFactory = new IntegerOperationsFactoryImpl();
            this.operations = new List<Pair<IIntegerOperation, int>>();
            this.opOptions = Enum.GetValues(typeof(Operations)).Cast<Operations>().ToList();
            this.same = false;
            this.maxOperation = null;
            this.Reset();

        }
 

        public int GetNumOfOperations()
        {
            return NUM_OPERATIONS_GAME;
        }

        public List<IIntegerOperation> GetOperations()
        {
            return this.operations.AsEnumerable().Select(e => e.GetFirst()).ToList();
        }

        public bool IsCorrectAnswer(string Answer)
        {
            return Answer == SAME_ANSWER && this.same
               || this.maxOperation != null && Answer == this.maxOperation.GetOperationAsString();
        }

        private void generateOperation(Operations opType)
        {
            IIntegerOperation operation = null;
            switch (opType)
            {
                case Model.Operations.ADDICTION:
                    operation = this.opFactory.Addiction(NUM_OPERAND, UPPER_BOUND);
                    break;
                case Model.Operations.SUBTRACTION:
                    operation =this.opFactory.Subtraction(NUM_OPERAND, UPPER_BOUND);
                    break;
                case Model.Operations.DIVISION:
                    operation = this.opFactory.Division(NUM_OPERAND, UPPER_BOUND);
                    break;
                case Model.Operations.MULTIPLICATION:
                    operation = this.opFactory.Multiplication(NUM_OPERAND, UPPER_BOUND);
                    break;
                case Model.Operations.SUM_EXPRESSION:
                    operation = this.opFactory.SumExpression(NUM_OPERATION_EXPR, NUM_OPERAND, UPPER_BOUND);
                    break;
                case Model.Operations.SUB_EXPRESSION:
                    operation = this.opFactory.SubExpression(NUM_OPERATION_EXPR, NUM_OPERAND, UPPER_BOUND);
                    break;                
            }

            this.operations.Add(new Pair<IIntegerOperation, int>(operation, operation.GetResult()));
        }

        private void ComputeAnswer()
        {
            int count = 0;
            int maxRes = this.operations.AsEnumerable().Select(e => e.GetSecond()).Max();
            IIntegerOperation maxOpTemp = null;
            for (int i = 0; i < this.operations.Count; i++)
            {
                if (this.operations.ElementAt(i).GetSecond() == maxRes)
                {
                    count = count + 1;
                    maxOpTemp = operations.ElementAt(i).GetFirst();
                }
            }
            if (count == this.operations.Count)
            {
                this.same = true;
                this.maxOperation = null;
            }
            else
            {
                this.same = false;
                this.maxOperation = maxOpTemp;
            }
        }

        public void Reset()
        {
            Random rnd = new Random();
            this.operations.Clear();
            for (int i = 0; i < NUM_OPERATIONS_GAME; i++)
            {
                this.generateOperation(this.opOptions.ElementAt(rnd.Next(this.opOptions.Count - 1)));
            }

            this.ComputeAnswer();
        }
    }
}
