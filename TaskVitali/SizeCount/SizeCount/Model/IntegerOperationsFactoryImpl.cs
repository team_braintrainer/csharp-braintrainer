﻿namespace SizeCount.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The implementation of the <see cref="IDividers"/>..
    /// </summary>
    public class IntegerOperationsFactoryImpl : IIntegerOperationsFactory
    {
        private readonly Random rnd;

        public IntegerOperationsFactoryImpl()
        {
            this.rnd = new Random();
        }

        public IIntegerOperation Addiction(int numOfOperand, int bound)
        {
            List<int> operands = this.GenerateRandomOperand(numOfOperand, bound);
            IIntegerOperation op = new IntegerOperationImpl(operands.AsEnumerable().Sum(), this.GenerateOperationString(operands,Operator.SUM));
            return op;
        }

        public IIntegerOperation Division(int numOfOperand, int bound)
        {
            List<int> operands = this.GenerateRandomOperandDivision(numOfOperand, bound);
            IIntegerOperation op = new IntegerOperationImpl(operands.AsEnumerable().Aggregate((e1, e2) => e1 / e2), this.GenerateOperationString(operands, Operator.DIV));
            return op;
        }

        public IIntegerOperation Multiplication(int numOfOperand, int bound)
        {
            List<int> operands = this.GenerateRandomOperand(numOfOperand, bound);
            IIntegerOperation op = new IntegerOperationImpl(operands.AsEnumerable().Aggregate((e1, e2) => e1 * e2), this.GenerateOperationString(operands, Operator.MUL));
            return op;
        }

        public IIntegerOperation SubExpression(int numOfOperations, int numOfOperand, int bound)
        {
            List<IIntegerOperation> operands = this.GenerateRandomOperation(numOfOperations, numOfOperand, bound);
            int result = operands.Select(e => e.GetResult()).Aggregate((r1,r2) => r1-r2);
            string opString = "(" + operands.ElementAt(0).GetOperationAsString() + ")";
            for (int i = 1; i < operands.Count; i++)
            {
                opString = opString + " - " + "(" + operands.ElementAt(i).GetOperationAsString() + ")";
            }

            return new IntegerOperationImpl(result, opString);
        }

        public IIntegerOperation Subtraction(int numOfOperand, int bound)
        {
            List<int> operands = this.GenerateRandomOperand(numOfOperand, bound);
            IIntegerOperation op = new IntegerOperationImpl(operands.AsEnumerable().Aggregate((e1,e2) => e1- e2), this.GenerateOperationString(operands, Operator.SUB));
            return op;
        }

        public IIntegerOperation SumExpression(int numOfOperations, int numOfOperand, int bound)
        {
            List<IIntegerOperation> operands = this.GenerateRandomOperation(numOfOperations, numOfOperand, bound);
            int result = operands.Select(e => e.GetResult()).Sum();
            string opString = "(" + operands.ElementAt(0).GetOperationAsString() + ")";
            for (int i = 1; i < operands.Count; i++)
            {
                opString = opString + " + " + "(" + operands.ElementAt(i).GetOperationAsString() + ")";
            }
            return new IntegerOperationImpl(result, opString);
        }

        private List<int> GenerateRandomOperand(int numOfOperands, int bound)
        {
            return Enumerable.Range(0, numOfOperands).Select(e => this.rnd.Next(bound)).ToList();
        }

        private List<int> GenerateRandomOperandDivision(int numOfOperand, int bound)
        {
            IDividers div = new DividersImpl();
            List<int> result = new List<int>();
            int num = this.rnd.Next(bound) + 1;
            result.Add(num);
            for (int i = 0; i < numOfOperand; i++)
            {
                List<int> dividersList = div.GetDividers(num);
                int divider = dividersList.ElementAt(this.rnd.Next(dividersList.Count - 1));
                result.Add(divider);
                num = num / divider;

            }
            return result.AsEnumerable().Take(numOfOperand).ToList();
        }

        private List<IIntegerOperation> GenerateRandomOperation(int numOfOperations, int numOfOperand, int bound)
        {
            List<Operator> operators = Enum.GetValues(typeof(Operator)).Cast<Operator>().ToList();
            List<IIntegerOperation> list = new List<IIntegerOperation>();
            for (int i = numOfOperations; i > 0; i--)
            {
                Operator op = operators.ElementAt(this.rnd.Next(operators.Count - 1));
                switch (op)
                {
                    case Operator.SUM:
                        list.Add(this.Addiction(numOfOperand, bound));
                        break;
                    case Operator.SUB:
                        list.Add(this.Subtraction(numOfOperand, bound));
                        break;
                    case Operator.DIV:
                        list.Add(this.Division(numOfOperand, bound));
                        break;
                    case Operator.MUL:
                        list.Add(this.Multiplication(numOfOperand, bound));
                        break;

                }
            }
            return list;
        }

        private string GenerateOperationString(List<int> operands, Operator operatorType)
        {
            string separator = string.Empty;
            switch (operatorType)
            {
                case Operator.SUM:
                    separator = "+";
                    break;
                case Operator.SUB:
                    separator = "-";
                    break;
                case Operator.MUL:
                    separator = "*";
                    break;
                case Operator.DIV:
                    separator = "/";
                    break;

            }

            string result = operands.ElementAt(0).ToString();

            for (int i = 1; i < operands.Count; i++)
            {
                result = result + " " + separator + " " + operands.ElementAt(i).ToString();
            }
            return result;
        }
    }
}
