﻿namespace SizeCount.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A model interface for the game size count.
    /// </summary>
    public interface ISizeCountModel
    {
        /// <summary>
        /// Verify if the answer is correct.
        /// </summary>
        /// <param name="answer">the answer to check</param>
        /// <returns>true if the answer is correct false otherwise</returns>
        bool IsCorrectAnswer(string answer);

        /// <summary>
        /// Get the random operations that compose the game.
        /// </summary>
        /// <returns>the list of  <see cref="INtegerOperation"/> that compose the game</returns>
        List<IIntegerOperation> GetOperations();

        /// <summary>
        /// Reset the game and compute new operations.
        /// </summary>
        void Reset();

        /// <summary>
        /// Get the number of operations that are involved in the game.
        /// </summary>
        /// <returns>the number of operations of which the game is made up</returns>
        int GetNumOfOperations();
    }
}
