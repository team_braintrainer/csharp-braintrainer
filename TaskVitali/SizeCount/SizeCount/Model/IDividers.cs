﻿namespace SizeCount.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// A interface to compute dividers of a number.
    /// </summary>
    public interface IDividers
    {
        /// <summary>
        /// Get dividers of a number.
        /// </summary>
        /// <param name="number">the number whose dividers you want to know</param>
        /// <returns>the list of dividers of the number specify</returns>
        List<int> GetDividers(int number);
    }
}
