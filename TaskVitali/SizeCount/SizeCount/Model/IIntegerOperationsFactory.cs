﻿namespace SizeCount.Model
{
    /// <summary>
    /// A factory interface for mathematical operations.
    /// </summary>
    public interface IIntegerOperationsFactory
    {
        /// <summary>
        /// Generate an addition between random Integer.
        /// </summary>
        /// <param name="numOfOperand">the number of operands involved in the operation</param>
        /// <param name="bound">the max value that a number can take in the operation</param>
        /// <returns>a <see cref="INtegerOperation"/> which is an addiction</returns>
        IIntegerOperation Addiction(int numOfOperand, int bound);

        /// <summary>
        /// Generate an subtraction between random Integer.
        /// </summary>
        /// <param name="numOfOperand">the number of operands involved in the operation</param>
        /// <param name="bound">the max value that a number can take in the operation</param>
        /// <returns>a <see cref="INtegerOperation"/> which is a subtraction</returns>
        IIntegerOperation Subtraction(int numOfOperand, int bound);

        /// <summary>
        /// Generate an multiplication between random Integer.
        /// </summary>
        /// <param name="numOfOperand">the number of operands involved in the operation</param>
        /// <param name="bound">the max value that a number can take in the operation</param>
        /// <returns>a <see cref="INtegerOperation"/> which is a multiplication</returns>
        IIntegerOperation Multiplication(int numOfOperand, int bound);

        /// <summary>
        /// Generate an division between random Integer.
        /// </summary>
        /// <param name="numOfOperand">the number of operands involved in the operation</param>
        /// <param name="bound">the max value that a number can take in the operation</param>
        /// <returns>a <see cref="INtegerOperation"/> which is a division</returns>
        IIntegerOperation Division(int numOfOperand, int bound);

        /// <summary>
        /// Generate an expression of random <see cref="INtegerOperation"/> united by an addiction.
        /// </summary>
        /// <param name="numOfOperations">the number of random <see cref="INtegerOperation"/> involved in the expression</param>
        /// <param name="numOfOperand">the number of operands involved in each <see cref="INtegerOperation"/> that compose the expression</param>
        /// <param name="bound">the max value that a number can take in the operation</param>
        /// <returns>a <see cref="INtegerOperation"/> which is a sum Expression</returns>
        IIntegerOperation SumExpression(int numOfOperations, int numOfOperand, int bound);

        /// <summary>
        /// Generate an expression of random <see cref="INtegerOperation"/> united by a subtraction.
        /// </summary>
        /// <param name="numOfOperations">the number of random <see cref="INtegerOperation"/> involved in the expression</param>
        /// <param name="numOfOperand">>the number of operands involved in each <see cref="INtegerOperation"/> that compose the expression</param>
        /// <param name="bound">the max value that a number can take in the operation</param>
        /// <returns>a <see cref="INtegerOperation"/> which is a sub Expression</returns>
        IIntegerOperation SubExpression(int numOfOperations, int numOfOperand, int bound);
    }
}
