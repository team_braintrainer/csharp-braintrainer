﻿namespace SizeCount.Model
{
    using System;

    /// <summary>
    /// The implementation of the <see cref="IIntegerOperation"/>.
    /// </summary>
    public class IntegerOperationImpl : IIntegerOperation
    {
        private readonly int result;
        private readonly string operationString;

        public IntegerOperationImpl(int result, string operationString)
        {
            this.result = result;
            this.operationString = operationString;
        }

        public string GetOperationAsString()
        {
            return this.operationString;
        }

        public int GetResult()
        {
            return this.result;
        }
    }
}
