﻿namespace SizeCount.Model
{
    using System;

    /// <summary>
    /// A interface represent an operation between integers.
    /// </summary>
    public interface IIntegerOperation
    {
        /// <summary>
        /// Compute the result of the operation.
        /// </summary>
        /// <returns>the result of the operation</returns>
        int GetResult();

        /// <summary>
        /// Represent the operation as a String
        /// </summary>
        /// <returns>the String that represent the operations</returns>
        string GetOperationAsString();
    }
}
