﻿namespace SizeCount.View
{
    using SizeCount.Model;
    using System.Collections.Generic;

    /// <summary>
    /// A interface view for the game size count.
    /// </summary>
    public interface ISizeCountView
    {
        /// <summary>
        /// Draw the view of the minigame.
        /// </summary>
        /// <param name="operations">perations that compose the minigame</param>
        void Draw(List<IIntegerOperation> operations);
    }
}
