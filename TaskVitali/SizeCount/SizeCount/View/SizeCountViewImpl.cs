﻿namespace Prova.View
{
    using SizeCount.Controller;
    using SizeCount.Model;
    using SizeCount.View;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// The implemenation of <see cref="ISizeCountView"/>.
    /// </summary>
    public partial class SizeCountViewImpl : Form, ISizeCountView
    {
        private const int NUM_OPERATION = 2;
        private ISizeCountController controller;
        private List<Button> buttons;

        public SizeCountViewImpl()
        {
            this.buttons = new List<Button>();
            InitializeComponent();
        }

        public void Draw(List<IIntegerOperation> operations)
        {
            for (int i = 0; i < NUM_OPERATION; i++)
            {
                this.buttons.ElementAt(i).Text = operations.ElementAt(i).GetOperationAsString();
            }
            
        }

        private void Button_Cliecked(object sender, EventArgs e)
        {
            var bt = sender as Button;
            this.controller.NewAttempt(bt.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
    

            for (int i = 0; i < NUM_OPERATION; i++)
            {
                Button bt = new Button();
                bt.AutoSize = true;
                bt.Click += new EventHandler(this.Button_Cliecked);
                this.TableLayoutPanel1.Controls.Add(bt, i, 0);
                this.buttons.Add(bt);
                
            }
            Button same = new Button();
            same.Text = "SAME";
            same.AutoSize = true;
            same.Click += new EventHandler(this.Button_Cliecked);
            this.TableLayoutPanel1.Controls.Add(same, NUM_OPERATION + 1, 0);
            this.controller = new SizeCountControllerImpl(this);
        }

        
    }
}
