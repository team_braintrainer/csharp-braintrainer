namespace UnitTest
{
    using System;
    using ColorGamaModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Test for the <see cref="ColorValuesIterator"/> class.
    /// </summary>
    [TestClass]
    public class TestColorValuesIterator
    {
        private static readonly int NEGATIVE_SIZE = -5;
        private static readonly int ZERO_SIZE = 0;
        private IColorValuesIterator iterator;
        private int size;
        private int numColors;

        /// <summary>
        /// A standard behavior test.
        /// </summary>
        [TestMethod]
        public void TestStandardBehavior()
        {
            this.TestInitialize(DifficultyLevel.EASY, QuestionType.COLOR);
            for (int i = 0; i < this.size; i++)
            {
                Assert.IsTrue(this.iterator.GetEnumerator().MoveNext());
            }
        }

        /// <summary>
        /// Test exception when call an invalid operation.
        /// </summary>
        [TestMethod]
        public void TestInvalidOperationException()
        {
            this.TestInitialize(DifficultyLevel.EASY, QuestionType.COLOR);
            for (int i = 0; i < this.size; i++)
            {
                this.iterator.GetEnumerator().MoveNext();
            }

            Assert.ThrowsException<InvalidOperationException>(() => this.iterator.GetEnumerator().MoveNext());
            Assert.ThrowsException<InvalidOperationException>(() => this.iterator.GetEnumerator().MoveNext());
        }

        /// <summary>
        /// Test exception when passed an illegal argument.
        /// </summary>
        [TestMethod]
        public void TestIllegalArgumentException()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ColorValues(NEGATIVE_SIZE, this.numColors, new HSBColorValuesCalculator(DifficultyLevel.EASY)));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ColorValues(ZERO_SIZE, this.numColors, new HSBColorValuesCalculator(DifficultyLevel.EASY)));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ColorValues(this.size, NEGATIVE_SIZE, new HSBColorValuesCalculator(DifficultyLevel.EASY)));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ColorValues(this.size, ZERO_SIZE, new HSBColorValuesCalculator(DifficultyLevel.EASY)));
        }

        /// <summary>
        /// Initialize the iterator.
        /// </summary>
        /// <param name="difficulty">the current difficulty</param>
        /// <param name="question">the current question</param>
        private void TestInitialize(DifficultyLevel difficulty, QuestionType question)
        {
            IColorGamaSettings settings = new ColorGamaSettings(difficulty);
            this.size = settings.GridSize * settings.GridSize;
            this.numColors = settings.NumColor;
            IColorValues iteratorAggregate = new ColorValues(this.size, this.numColors, new HSBColorValuesCalculator(difficulty));
            iteratorAggregate.ResetValues(question);
            this.iterator = iteratorAggregate.Iterator;
        }
    }
}
