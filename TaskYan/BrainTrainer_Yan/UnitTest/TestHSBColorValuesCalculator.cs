﻿namespace UnitTest
{ 
    using System;
    using System.Collections.Generic;
    using ColorGamaModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Test for the <see cref="HSBColorValuesCalculator"/> class.
    /// </summary>
    [TestClass]
    public class TestHSBColorValuesCalculator
    {
        private static readonly decimal DEGREE = 360;
        private static readonly decimal ORIGIN = 0;
        private static readonly decimal BOUND = 1;
        private IColorValuesCalculator colorValuesCalculator;

        /// <summary>
        /// A standard behavior test.
        /// </summary>
        [TestMethod]
        public void TestStandardBehavior()
        {
            this.colorValuesCalculator = new HSBColorValuesCalculator(DifficultyLevel.EASY);
            Assert.IsTrue(this.colorValuesCalculator.CalculateRandomColorValue().Item1 <= DEGREE);
            Assert.IsTrue(this.colorValuesCalculator.CalculateRandomColorValue().Item2 > ORIGIN);
            Assert.IsTrue(this.colorValuesCalculator.CalculateRandomColorValue().Item2 < BOUND);
            Assert.IsTrue(this.colorValuesCalculator.CalculateRandomColorValue().Item3 > ORIGIN);
            Assert.IsTrue(this.colorValuesCalculator.CalculateRandomColorValue().Item3 < BOUND);
        }

        /// <summary>
        /// Test exception when passed a null argument.
        /// </summary>
        [TestMethod]
        public void TestArgumentNullException()
        {
            this.colorValuesCalculator = new HSBColorValuesCalculator(DifficultyLevel.EASY);
            HashSet<Tuple<decimal, decimal, decimal>> set = new HashSet<Tuple<decimal, decimal, decimal>>();
            Assert.ThrowsException<ArgumentNullException>(() => this.colorValuesCalculator.CalculateRightColor(QuestionType.COLOR, null));
            Assert.ThrowsException<ArgumentException>(() => this.colorValuesCalculator.CalculateRightColor(QuestionType.DARKEST, set));
            set.Add(this.colorValuesCalculator.CalculateRandomColorValue());
            Assert.ThrowsException<ArgumentNullException>(() => this.colorValuesCalculator.CalculateRightColor(null, set));
            this.colorValuesCalculator.CalculateRightColor(QuestionType.LIGHTEST, set);
        }
    }
}