﻿namespace ColorGamaModel
{
    using System;
    using System.Collections;

    /// <summary>
    /// This interface represents color values of the mini-game to iterate.
    /// </summary>
    public interface IColorValuesIterator : IEnumerable
    {
        /// <summary>
        /// Gets the index of the the right color.
        /// </summary>
        int RightColorIndex { get; }

        /// <summary>
        /// Gets or sets the right color values.
        /// </summary>
        Tuple<decimal, decimal, decimal> RightColorValues { get; set; }
    }
}
