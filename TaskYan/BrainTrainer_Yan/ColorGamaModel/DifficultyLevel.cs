﻿namespace ColorGamaModel
{
    /// <summary>
    /// The possible difficulty of the mini-game.
    /// </summary>
    public enum DifficultyLevel
    {
        /// <summary>
        /// Easy level.
        /// </summary>
        EASY,

        /// <summary>
        /// Normal level.
        /// </summary>
        NORMAL,

        /// <summary>
        /// Hard level.
        /// </summary>
        HARD
    }
}
