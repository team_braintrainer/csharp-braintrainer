﻿namespace ColorGamaModel
{
    /// <summary>
    /// This class implements <see cref="IHsbValueSetting"/>
    /// </summary>
    public class HSBValuesSetting : IHsbValueSetting
    {
        private static readonly decimal ORIGIN = 30;
        private static readonly decimal BOUND = 1;
        private static readonly decimal HUNDRED = 100;
        private static readonly decimal ORIGIN_VALUE = 20;
        private static readonly decimal OFFSET = 10;

        /// <summary>
        /// Initializes a new instance of the <see cref="HSBValuesSetting"/> class.
        /// </summary>
        /// <param name="difficulty">the difficulty of the mini-game</param>
        public HSBValuesSetting(DifficultyLevel difficulty)
        {
            decimal Operation(decimal i1, decimal i2) => (i1 * (decimal)difficulty) + i2;
            this.HueRange = Operation(-BOUND, ORIGIN_VALUE);
            this.ValueRange = Operation(-BOUND, OFFSET) / HUNDRED;
        }

        ///<inheritdoc/>
        public decimal HueRange { get; }

        ///<inheritdoc/>
        public decimal ValueRange { get; }

        ///<inheritdoc/>
        public decimal RandomColorOrigin => (decimal)ORIGIN / HUNDRED;

        ///<inheritdoc/>
        public decimal RandomColorBound => BOUND - this.ValueRange;
    }
}
