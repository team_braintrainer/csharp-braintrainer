﻿namespace ColorGamaModel
{
    /// <summary>
    /// Enumeration of possible methods to represent colors.
    /// </summary>
    public enum ColorMethod
    {
        /// <summary>
        /// RGB color value.
        /// </summary>
        RGB_VALUE,

        /// <summary>
        /// HSB color value.
        /// </summary>
        HSB_VALUE
    }
}