﻿namespace ColorGamaModel
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This interface calculates the values of the colors for the mini-game.
    /// </summary>
    public interface IColorValuesCalculator
    {
        /// <summary>
        /// Gets the method of color value representations.
        /// </summary>
        ColorMethod ColorMethod { get; }

        /// <summary>
        /// Calculate the right color values according to the question type from the set.
        /// </summary>
        /// <param name="question">the current question</param>
        /// <param name="colorValuesSet">the set to calculate</param>
        /// <returns>the right color value</returns>
        Tuple<decimal, decimal, decimal> CalculateRightColor(QuestionType question, HashSet<Tuple<decimal, decimal, decimal>> colorValuesSet);

        /// <summary>
        /// Calculate a random color value.
        /// </summary>
        /// <returns>the color values</returns>
        Tuple<decimal, decimal, decimal> CalculateRandomColorValue();

        /// <summary>
        /// Reset all values.
        /// </summary>
        void Reset();
    }
}
