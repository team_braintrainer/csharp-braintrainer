﻿namespace ColorGamaModel
{
    using System;

    /// <summary>
    /// Program class.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The entry point for the application.
        /// </summary>
        /// <param name="args">a list of command line arguments</param>
        public static void Main(string[] args)
        {
            Console.WriteLine("Task c-sharp Brain Trainer");
        }
    }
}
