﻿namespace ColorGamaModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// This class implements <see cref="IColorValuesIterator"/> interface.
    /// </summary>
    public class ColorValuesIterator : IColorValuesIterator
    {
        private readonly HashSet<Tuple<decimal, decimal, decimal>> colorValuesSet;
        private readonly int size;
        private int counter;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorValuesIterator"/> class.
        /// </summary>
        /// <param name="size">the size of the iterator</param>
        /// <param name="colorValuesSet">the set to iterate</param>
        public ColorValuesIterator(int size, HashSet<Tuple<decimal, decimal, decimal>> colorValuesSet)
        {
            this.Check(size);
            this.colorValuesSet = colorValuesSet;
            this.size = size;
            this.RightColorIndex = new Random().Next(this.size);
            this.counter = 0;
        }

        ///<inheritdoc/>
        public int RightColorIndex { get; private set; }

        ///<inheritdoc/>
        public Tuple<decimal, decimal, decimal> RightColorValues { get; set; }

        ///<inheritdoc/>
        public IEnumerator GetEnumerator()
        {
            if (!(this.counter < this.size))
            {
                throw new InvalidOperationException();
            }

            yield return (this.counter++ == this.RightColorIndex) ? this.RightColorValues : this.colorValuesSet.ElementAt(new Random().Next(this.colorValuesSet.Count));
        }

        private void Check(int size)
        {
            if (size <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}
