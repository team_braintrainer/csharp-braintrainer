﻿namespace ColorGamaModel
{
    /// <summary>
    /// This interface calculates constants according to the difficulty.
    /// </summary>
    public interface IColorGamaSettings
    {
        /// <summary>
        /// Gets the grid size.
        /// </summary>
        int GridSize { get; }

        /// <summary>
        /// Gets the time duration.
        /// </summary>
        int Seconds { get; }

        /// <summary>
        /// Gets the base point of mini-game.
        /// </summary>
        int BasePoint { get; }

        /// <summary>
        /// Gets the number of different color to show.
        /// </summary>
        int NumColor { get; }
    }
}
