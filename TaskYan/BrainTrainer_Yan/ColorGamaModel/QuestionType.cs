﻿namespace ColorGamaModel
{
    /// <summary>
    /// This class describe all types of questions.
    /// </summary>
    public class QuestionType
    {
        /// <summary>
        /// The darkest color.
        /// </summary>
        public static readonly QuestionType DARKEST = new QuestionType("Find the darkest color");

        /// <summary>
        /// The lightest color.
        /// </summary>
        public static readonly QuestionType LIGHTEST = new QuestionType("Find the lightest color");

        /// <summary>
        /// The specific color.
        /// </summary>
        public static readonly QuestionType COLOR = new QuestionType("Find the color indicated");

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionType"/> class.
        /// </summary>
        /// <param name="question">the string question name associated</param>
        private QuestionType(string question) => this.QuestionName = question;

        /// <summary>
        /// Gets the name of the question.
        /// </summary>
        public string QuestionName { get; private set; }
    }
}