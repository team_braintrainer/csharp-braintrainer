﻿namespace ColorGamaModel
{
    /// <summary>
    /// This interface represents the color values of the mini-game.
    /// </summary>
    public interface IColorValues
    {
        /// <summary>
        /// Gets the color values iterator.
        /// </summary>
        IColorValuesIterator Iterator { get; }

        /// <summary>
        /// Gets the method of color value representations.
        /// </summary>
        ColorMethod ColorMethod { get; }

        /// <summary>
        /// Reset all values.
        /// </summary>
        /// <param name="question">the current question type</param>
        void ResetValues(QuestionType question);
    }
}
