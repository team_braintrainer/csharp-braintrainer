﻿namespace ColorGamaModel
{
    /// <summary>
    /// This class implements IColorGamaSettings
    /// </summary>
    public class ColorGamaSettings : IColorGamaSettings
    {
        private static readonly int BASE = 4;
        private static readonly int ORIGIN = 30;
        private static readonly int BASESECONDS = 5;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorGamaSettings"/> class.
        /// </summary>
        /// <param name="difficulty">the difficulty of the mini-game</param>
        public ColorGamaSettings(DifficultyLevel difficulty)
        {
            int Operation(int i1, int i2) => (i1 * (int)difficulty) + i2;
            this.GridSize = Operation(BASE, BASE);
            this.Seconds = Operation(BASESECONDS, ORIGIN);
            this.NumColor = Operation(BASE, BASE);
        }

        ///<inheritdoc/>
        public int GridSize { get; private set; }

        ///<inheritdoc/>
        public int Seconds { get; private set; }

        ///<inheritdoc/>
        public int BasePoint { get; private set; }

        ///<inheritdoc/>
        public int NumColor { get; private set; }
    }
}
