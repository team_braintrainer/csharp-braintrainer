﻿namespace ColorGamaModel
{
    /// <summary>
    /// The model interface of ColorGama mini-game.
    /// </summary>
    public interface IColorGamaModel
    {
        /// <summary>
        /// Gets the constants according to the difficulty.
        /// </summary>
        IColorGamaSettings DifficultyValue { get; }

        /// <summary>
        /// Gets the color values iterator.
        /// </summary>
        IColorValuesIterator ColorIterator { get; }

        /// <summary>
        /// Gets the type of current question.
        /// </summary>
        QuestionType CurrentQuestion { get; }

        /// <summary>
        /// Gets the method of color value representations.
        /// </summary>
        ColorMethod ColorMethod { get; }

        /// <summary>
        /// Set the next question of the mini-game.
        /// </summary>
        /// <returns>the next question</returns>
        QuestionType GetNextQuestion();
    }
}