﻿namespace ColorGamaModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// This class implements <see cref="IColorValuesCalculator"/> interface.
    /// </summary>
    public class HSBColorValuesCalculator : IColorValuesCalculator
    {
        private static readonly decimal DEGREE = 361;

        private readonly IHsbValueSetting valueSetting;
        private readonly Random rand;
        private decimal hue;

        /// <summary>
        /// Initializes a new instance of the <see cref="HSBColorValuesCalculator"/> class.
        /// </summary>
        /// <param name="difficulty">the current difficulty</param>
        public HSBColorValuesCalculator(DifficultyLevel difficulty)
        {
            this.valueSetting = new HSBValuesSetting(difficulty);
            this.rand = new Random();
        }

        ///<inheritdoc/>
        public ColorMethod ColorMethod => ColorMethod.HSB_VALUE;

        ///<inheritdoc/>
        public Tuple<decimal, decimal, decimal> CalculateRandomColorValue() => new Tuple<decimal, decimal, decimal>(
                                                    this.hue, this.CalculateRandomValue(), this.CalculateRandomValue());

        ///<inheritdoc/>
        public Tuple<decimal, decimal, decimal> CalculateRightColor(QuestionType question, HashSet<Tuple<decimal, decimal, decimal>> colorValuesSet)
        {
            this.Check(question, colorValuesSet);

            if (question == QuestionType.DARKEST)
            {
                return new Tuple<decimal, decimal, decimal>(
                    this.hue,
                    colorValuesSet.Max(c => c.Item2) + this.valueSetting.ValueRange,
                    colorValuesSet.Min(c => c.Item3) - this.valueSetting.ValueRange);
            }
            else if (question == QuestionType.LIGHTEST)
            {
                return new Tuple<decimal, decimal, decimal>(
                    this.hue,
                    colorValuesSet.Min(c => c.Item2) - this.valueSetting.ValueRange,
                    colorValuesSet.Max(c => c.Item3) + this.valueSetting.ValueRange);
            }
            else if (question == QuestionType.COLOR)
            {
                return new Tuple<decimal, decimal, decimal>(
                    this.hue + this.valueSetting.HueRange,
                    this.CalculateRandomValue(),
                    this.CalculateRandomValue());
            }
            else
            {
                return null;
            }
        }

        ///<inheritdoc/>
        public void Reset() => this.hue = this.rand.Next(Decimal.ToInt16(DEGREE - this.valueSetting.HueRange));

        private decimal CalculateRandomValue() => (Convert.ToDecimal(this.rand.NextDouble()) * (this.valueSetting.RandomColorBound
                                                                         - this.valueSetting.RandomColorOrigin))
                                                                         + this.valueSetting.RandomColorOrigin;

        private void Check(QuestionType question, HashSet<Tuple<decimal, decimal, decimal>> colorValuesSet)
        {
            if (question == null)
            {
                throw new ArgumentNullException(nameof(question));
            }

            if (colorValuesSet == null)
            {
                throw new ArgumentNullException(nameof(colorValuesSet));
            }

            if (!colorValuesSet.Any())
            {
                throw new ArgumentException(nameof(colorValuesSet));
            }
        }
    }
}
