﻿namespace ColorGamaModel
{
    /// <summary>
    /// This interface represents the setting of the HSB color values.
    /// </summary>
    public interface IHsbValueSetting
    {
        /// <summary>
        /// Gets the random origin of the color value.
        /// </summary>
        decimal RandomColorOrigin { get; }

        /// <summary>
        /// Gets the random bound of the color value.
        /// </summary>
        decimal RandomColorBound { get; }

        /// <summary>
        /// Gets the range of the hue component.
        /// </summary>
        decimal HueRange { get; }

        /// <summary>
        /// Gets the range of the saturation or brightness component.
        /// </summary>
        decimal ValueRange { get; }
    }
}
