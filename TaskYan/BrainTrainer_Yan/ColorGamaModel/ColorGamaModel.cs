﻿namespace ColorGamaModel
{
    using System;

    /// <summary>
    /// This class implements <see cref="IColorGamaModel"/> interface.
    /// </summary>
    public class ColorGamaModel : IColorGamaModel
    {
        private readonly IColorGamaSettings computeValue;
        private readonly IColorValues colorValues;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorGamaModel"/> class.
        /// </summary>
        /// <param name="difficulty">the current difficulty</param>
        public ColorGamaModel(DifficultyLevel difficulty)
        {
            this.computeValue = new ColorGamaSettings(difficulty);
            this.colorValues = new ColorValues(this.computeValue.GridSize * this.computeValue.GridSize, this.computeValue.NumColor, new HSBColorValuesCalculator(difficulty));
        }

        ///<inheritdoc/>
        public IColorGamaSettings DifficultyValue { get; }

        ///<inheritdoc/>
        public IColorValuesIterator ColorIterator { get; }

        ///<inheritdoc/>
        public QuestionType CurrentQuestion { get; private set; }

        ///<inheritdoc/>
        public ColorMethod ColorMethod => this.colorValues.ColorMethod;

        ///<inheritdoc/>
        public QuestionType GetNextQuestion()
        {
            Array values = Enum.GetValues(typeof(QuestionType));
            this.CurrentQuestion = (QuestionType)values.GetValue(new Random().Next(values.Length));
            return this.CurrentQuestion;
        }
    }
}
