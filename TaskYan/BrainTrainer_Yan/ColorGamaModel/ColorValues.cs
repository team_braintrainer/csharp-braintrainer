﻿namespace ColorGamaModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// This class implements <see cref="IColorValues"/> interface.
    /// </summary>
    public class ColorValues : IColorValues
    {
        private readonly HashSet<Tuple<decimal, decimal, decimal>> colorValuesSet;
        private readonly IColorValuesCalculator colorValuesCalculator;
        private readonly int numColors;
        private readonly int size;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorValues"/> class.
        /// </summary>
        /// <param name="size">the size of the iterator</param>
        /// <param name="numColors">the number of colors</param>
        /// <param name="calculator">the color values calculator</param>
        public ColorValues(int size, int numColors, IColorValuesCalculator calculator)
        {
            this.Check(size);
            this.Check(numColors);
            this.colorValuesCalculator = calculator ?? throw new ArgumentNullException(nameof(calculator));
            this.colorValuesSet = new HashSet<Tuple<decimal, decimal, decimal>>();
            this.numColors = numColors;
            this.size = size;
        }

        ///<inheritdoc/>
        public IColorValuesIterator Iterator { get; private set; }

        ///<inheritdoc/>
        public ColorMethod ColorMethod => this.colorValuesCalculator.ColorMethod;

        ///<inheritdoc/>
        public void ResetValues(QuestionType question)
        {
            this.colorValuesSet.Clear();
            this.colorValuesCalculator.Reset();
            this.PopulateSet();
            this.Iterator = new ColorValuesIterator(this.size, this.colorValuesSet)
            {
                RightColorValues = this.colorValuesCalculator.CalculateRightColor(question, this.colorValuesSet)
            };
        }

        private void Check(int size)
        {
            if (size <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }
        }

        private void PopulateSet()
        {
            while (this.colorValuesSet.Count() < this.numColors)
            {
                this.colorValuesSet.Add(this.colorValuesCalculator.CalculateRandomColorValue());
            }
        }
    }
}
