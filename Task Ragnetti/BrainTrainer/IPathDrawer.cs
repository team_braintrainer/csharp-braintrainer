namespace BrainTrainer_Ragnetti
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for drawing a particular path. 
    /// </summary>
    public interface IPathDrawer
    {
        /// <summary>
        /// Generate the path that links the initial to the final position.
        /// </summary>
        void DrawPath();

        /// <summary>
        /// Getter for the initial position.
        /// </summary>
        /// <returns>the initial position </returns>
        Pair<int, int> GetInitialPosition();

        /// <summary>
        /// Getter for the path.
        /// </summary>
        /// <returns> the complete drawn path </returns>
        List<Direction> GetSteps();

        /// <summary>
        /// Getter for the final position.
        /// </summary>
        /// <returns> the final position </returns>
        Pair<int, int> GetFinalPosition();
    }
}