﻿namespace BrainTrainer_Ragnetti
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Implementation of OneWayModel.
    /// </summary>
    public class OneWayModelImpl : IOneWayModel
    {
        private const int Arrows = 2;
        private const int GridSize = 4;

        private readonly DifficultyLevel difficulty;
        private readonly OneWayPathDrawerImpl pathDrawer;

        /// <summary>
        /// Initialize a new instance of the <see cref="OneWayModelImpl"/> class.
        /// </summary>
        /// <param name="difficulty"> the selected difficulty </param>
        public OneWayModelImpl(DifficultyLevel difficulty)
        {
            this.difficulty = difficulty;
            this.pathDrawer = new OneWayPathDrawerImpl(GridSize, Arrows);
        }

        /// <inheritdoc />
        public void OneWayInit() => this.pathDrawer.DrawPath();

        /// <inheritdoc />
        public List<Direction> GetSteps() => this.pathDrawer.GetSteps();

        /// <inheritdoc />
        public Pair<int, int> GetInitialPosition() => this.pathDrawer.GetInitialPosition();

        /// <inheritdoc />
        public Pair<int, int> GetFinalPosition() => this.pathDrawer.GetFinalPosition();

        /// <inheritdoc />
        public DifficultyLevel GetDifficulty() => this.difficulty;

        /// <inheritdoc />
        public int GetGridSize() => GridSize;
    }
}