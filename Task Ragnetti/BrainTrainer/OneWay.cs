namespace BrainTrainer_Ragnetti
{
    using System.Collections.Generic;

    /// <summary>
    ///  The interface for the visual representation of One Way.
    /// </summary>
    public interface IOneWay
    {
        /// <summary>
        /// At first initialization, draw the arrows of the game.
        /// </summary>
        /// <param name="steps"> the steps to turn into arrows </param>
        void DrawArrows(List<Direction> steps);

        /// <summary>
        ///  At each level, update the arrows to draw.
        /// </summary>
        /// <param name="steps"> the steps to turn into arrows </param>
        void UpdateArrows(List<Direction> steps);

        /// <summary>
        /// At first initialization, draw the main grid for the component of the game.
        /// </summary>
        /// <param name="size"> the number of rows and columns of the main grid </param>
        void DrawGrid(int size);

        /// <summary>
        /// Show the initial position on the screen.
        /// </summary>
        /// <param name="initialPosition"> the initial position </param>
        void UpdateView(Pair<int, int> initialPosition);

        /// <summary>
        /// Show if the player guess the answer.
        /// </summary>
        /// <param name="finalPosition"> the final position </param>
        void ShowCorrectAnswer(Pair<int, int> finalPosition);
    }
}