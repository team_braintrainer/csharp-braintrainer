﻿namespace BrainTrainer_Ragnetti
{
    using System.Windows.Forms;

    /// <summary>
    /// This class represent part of the OneWay view.
    /// </summary>
    partial class OneWayView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Label titleLabel;
        private Panel panel;
        private Button next;
        private Label info;
        private Label result;

        /// <summary>
        /// Clean UP any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OneWayView));
            this.panel = new System.Windows.Forms.Panel();
            this.result = new System.Windows.Forms.Label();
            this.info = new System.Windows.Forms.Label();
            this.next = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.result);
            this.panel.Controls.Add(this.info);
            this.panel.Controls.Add(this.next);
            this.panel.Controls.Add(this.titleLabel);
            this.panel.Location = new System.Drawing.Point(0, 2);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(564, 584);
            this.panel.TabIndex = 0;
            // 
            // result
            // 
            this.result.Location = new System.Drawing.Point(29, 479);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(97, 94);
            this.result.TabIndex = 4;
            // 
            // info
            // 
            this.info.AutoSize = true;
            this.info.Location = new System.Drawing.Point(73, 67);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(436, 20);
            this.info.TabIndex = 1;
            this.info.Text = "Click one button and the right-down arrow to complete the level";
            // 
            // next
            // 
            this.next.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.next.Image = ((System.Drawing.Image)(resources.GetObject("next.Image")));
            this.next.Location = new System.Drawing.Point(480, 498);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(75, 75);
            this.next.TabIndex = 3;
            this.next.UseVisualStyleBackColor = true;
            this.next.Click += new System.EventHandler(this.NextLevel);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.titleLabel.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.titleLabel.ForeColor = System.Drawing.Color.RoyalBlue;
            this.titleLabel.Location = new System.Drawing.Point(219, 24);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(141, 43);
            this.titleLabel.TabIndex = 2;
            this.titleLabel.Text = "One Way";
            // 
            // OneWayView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 587);
            this.Controls.Add(this.panel);
            this.Name = "OneWayView";
            this.Text = "OneWayView";
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}