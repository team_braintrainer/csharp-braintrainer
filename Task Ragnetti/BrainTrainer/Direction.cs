namespace BrainTrainer_Ragnetti
{
    /// <summary>
    ///  Enumeration for the possible direction.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Up direction.
        /// </summary>
        UP,

        /// <summary>
        /// Down direction.
        /// </summary>
        DOWN,

        /// <summary>
        /// Left direction.
        /// </summary>
        LEFT,

        /// <summary>
        /// Right direction.
        /// </summary>
        RIGHT
    }
}