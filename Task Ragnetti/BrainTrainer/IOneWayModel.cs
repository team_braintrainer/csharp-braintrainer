namespace BrainTrainer_Ragnetti
{
    using System.Collections.Generic;

    /// <summary>
    /// The model interface of One Way mini-game.
    /// </summary>
    public interface IOneWayModel
    {
        /// <summary>
        /// Getter for the DifficultyLevel
        /// </summary>
        /// <returns> the DifficultyLevel </returns>
        DifficultyLevel GetDifficulty();

        /// <summary>
        /// Getter for the list of Direction.
        /// </summary>
        /// <returns> the list of generated Direction </returns>
        List<Direction> GetSteps();

        /// <summary>
        /// Getter for the initial position.
        /// </summary>
        /// <returns> the initial position </returns>
        Pair<int, int> GetInitialPosition();

        /// <summary> 
        /// Getter for the final position.
        /// </summary>
        /// <returns> the final position </returns>
        Pair<int, int> GetFinalPosition();

        /// <summary>
        /// Initialize One Way.
        /// </summary>
        void OneWayInit();

        /// <summary>
        /// Getter for the grid size.
        /// </summary>
        /// <returns> the number of rows and columns </returns>
        int GetGridSize();
    }
}