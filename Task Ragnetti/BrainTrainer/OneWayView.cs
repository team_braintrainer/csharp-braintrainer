﻿namespace BrainTrainer_Ragnetti
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;

    /// <summary>
    /// Partial class that represents the view of One Way mini-game.
    /// </summary>
    public partial class OneWayView : Form
    {
        private readonly Dictionary<Button, Pair<int, int>> buttons;
        private readonly List<Button> arrows;
        private IOneWayController controller;
        private Button clickedBtn;
        private Button correctBtn;
        private bool submittedAnswer;
       
        public OneWayView()
        {
            this.InitializeComponent();
            this.submittedAnswer = false;
            this.buttons = new Dictionary<Button, Pair<int, int>>();
            this.arrows = new List<Button>();
            this.InitGame(DifficultyLevel.EASY);
        }

        /// <inheritdoc />
        public void Hit(object sender, System.EventArgs e)
        {
            this.clickedBtn = (Button)sender;
            if (!this.submittedAnswer)
            {
                this.controller.ShowSolution();
                this.controller.ResetGame();
                this.submittedAnswer = true;
            }  
        }

        /// <inheritdoc />
        public void DrawGrid(int size)
        {
            int buttonWidth = 90;
            int buttonHeight = 90;
            int distance = 20;
            int start_x = 90;
            int start_y = 90;
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    Button btn = new Button
                    {
                        Top = start_x + ((x * buttonHeight) + distance),
                        Left = start_y + ((y * buttonWidth) + distance),
                        Width = buttonWidth,
                        Height = buttonHeight
                    };
                    btn.Click += new EventHandler(this.Hit);
                    this.panel.Controls.Add(btn);
                    this.buttons.Add(btn, new Pair<int, int>(x, y));
                }
            }
        }

        /// <inheritdoc />
        public void DrawArrows(List<Direction> steps)
        {
            int arrowWidth = 70;
            int arrowHeight = 70;
            int distance = 20;
            int start_x = 80;
            int start_y = 10;

            for (int idx = 0; idx < steps.Count; idx++)
            {
                Button btn = new Button
                {
                    Top = start_x + ((idx * arrowHeight) + distance),
                    Left = start_y + (((int)(idx - 0.5) * arrowWidth) + distance),
                    Width = arrowWidth,
                    Height = arrowHeight
                };
                this.panel.Controls.Add(btn);
                this.arrows.Add(btn);
                this.LoadArrow(this.arrows[idx], steps[idx]);
            }
        }

        /// <inheritdoc />
        public void UpdateArrows(List<Direction> steps)
        {
            Enumerable.Range(0, steps.Count).ToList().ForEach(x =>
            {
                this.LoadArrow(arrows[x], steps[x]);
            });  
        }

        /// <inheritdoc />
        public void UpdateView(Pair<int, int> initialPosition)
        {
            foreach (KeyValuePair<Button, Pair<int, int>> entry in this.buttons)
            {
                entry.Key.BackgroundImage = null;
            }

            Button initialPositionButton = buttons.FirstOrDefault(k => k.Value.GetX() == initialPosition.GetX() && k.Value.GetY() == initialPosition.GetY()).Key;
            initialPositionButton.BackgroundImage = BrainTrainer_Ragnetti.Properties.Resources.start;
            initialPositionButton.BackgroundImageLayout = ImageLayout.Stretch;
        }

        /// <inheritdoc />
        public void ShowCorrectAnswer(Pair<int, int> finalPosition)
        {
            this.correctBtn = this.buttons.FirstOrDefault(x => x.Value.GetX() == finalPosition.GetX() && x.Value.GetY() == finalPosition.GetY()).Key;                                           
            result.Image = this.buttons[correctBtn].GetX() == this.buttons[clickedBtn].GetX()
                           && this.buttons[correctBtn].GetY() == this.buttons[clickedBtn].GetY() ? 
                                                                                              BrainTrainer_Ragnetti.Properties.Resources.correct 
                                                                                            : BrainTrainer_Ragnetti.Properties.Resources.wrong;      
        }

        private void NextLevel(object sender, EventArgs e)
        {
            if (this.submittedAnswer)
            {
                this.controller.ResetGame();
                this.result.Image = null;
                this.submittedAnswer = false;
            }
        }

        private void LoadArrow(Button arrowBtn, Direction dir)
        {
            switch (dir)
            {
                case Direction.UP:
                    arrowBtn.BackgroundImage = Properties.Resources.UP;
                    break;
                case Direction.DOWN:
                    arrowBtn.BackgroundImage = Properties.Resources.DOWN;
                    break;
                case Direction.LEFT:
                    arrowBtn.BackgroundImage = Properties.Resources.LEFT;
                    break;
                case Direction.RIGHT:
                    arrowBtn.BackgroundImage = Properties.Resources.RIGHT;
                    break;
                default:
                    break;
            }

            arrowBtn.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void InitGame(DifficultyLevel difficultyLevel)
        {
            this.controller = new OneWayControllerImpl(this, difficultyLevel);
            this.controller.FirstInit();
        }
    }
}
