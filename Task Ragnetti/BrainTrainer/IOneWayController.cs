namespace BrainTrainer_Ragnetti
{
    /// <summary>
    /// The interface for the One Way controller. 
    /// </summary>
    public interface IOneWayController
    {
        /// <summary>
        /// Initialize the game at its first initialization. 
        /// </summary>
        void FirstInit();

        /// <summary>
        /// Reset the game to prepare the next level. 
        /// </summary>
        void ResetGame();

        /// <summary>
        /// Show if the player guess the answer.
        /// </summary>
        void ShowSolution();
    }
}