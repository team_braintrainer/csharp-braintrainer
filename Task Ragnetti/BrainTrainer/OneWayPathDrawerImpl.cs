namespace BrainTrainer_Ragnetti
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Implementation of OneWayPathDrawer. This class generates the path from
    /// an initial random position to a computed final position.
    /// </summary>
    public class OneWayPathDrawerImpl : IPathDrawer
    {
        private readonly int size;
        private readonly int arrows;
        private readonly List<Direction> steps;

        private Pair<int, int> currentPosition;
        private Pair<int, int> initialPosition;
        private Pair<int, int> finalPosition;
        private Pair<int, int> prevFinishPosition;

        /// <summary>
        /// Constructor of OneWayPathDrawerImpl.
        /// </summary>
        /// <param name="size"> the size of the main grid playground </param>
        /// <param name="arrows"> the number of steps to compute </param>
        public OneWayPathDrawerImpl(int size, int arrows)
        {
            this.size = size;
            this.arrows = arrows;
            this.steps = new List<Direction>();
        }      

        /// <inheritdoc />
        public void DrawPath()
        {
            this.EnsureValidStart();
            this.steps.Clear();
            Enumerable.Range(0, this.arrows).ToList().ForEach(i =>
            {
                var random = new Random();
                Direction dir = (Direction) random.Next(Enum.GetNames(typeof(Direction)).Length);
                Pair<int, int> step = this.GenerateStep(dir);
                
                while (this.CheckX(step.GetX()) || this.CheckY(step.GetY()))
                {
                    dir = (Direction) random.Next(Enum.GetNames(typeof(Direction)).Length);
                    step = this.GenerateStep(dir);
                }

                int x = step.GetX() + this.currentPosition.GetX();
                int y = step.GetY() + this.currentPosition.GetY();
                this.currentPosition = new Pair<int, int>(x, y);
                this.steps.Add(dir);
            });

            if (this.IsClosedPath(this.currentPosition))
            {
                this.DrawPath();
            }
            else
            {
                this.finalPosition = currentPosition;
                this.prevFinishPosition = currentPosition;
            }
        }

        /// <inheritdoc />
        public Pair<int, int> GetInitialPosition() => initialPosition;

        /// <inheritdoc />
        public List<Direction> GetSteps() => steps;

        /// <inheritdoc />
        public Pair<int, int> GetFinalPosition() => finalPosition;

        private Pair<int, int> GenerateStep(Direction dir)
        {
            return dir switch
            {
                Direction.UP => new Pair<int, int>(-1, 0),
                Direction.DOWN => new Pair<int, int>(1, 0),
                Direction.LEFT => new Pair<int, int>(0, -1),
                Direction.RIGHT => new Pair<int, int>(0, 1),
                _ => null,
            };
        }

        private bool CheckX(int x)
        {
            return x + this.currentPosition.GetX() < 0 || x + this.currentPosition.GetX() >= size;
        }

        private bool CheckY(int y)
        {
            return y + this.currentPosition.GetY() < 0 || y + this.currentPosition.GetY() >= size;
        }

        private bool IsClosedPath(Pair<int, int> currentPosition)
        {
            return currentPosition.GetX() == initialPosition.GetX()
                   && currentPosition.GetY() == initialPosition.GetY();
        }

        private void SetInitialPosition()
        {
            Random random = new Random();
            this.currentPosition = new Pair<int, int>(random.Next(this.size), random.Next(this.size));
            this.initialPosition = this.currentPosition;
        }

        private void EnsureValidStart()
        {
            do
            {
                this.SetInitialPosition();
            } while (this.initialPosition == this.prevFinishPosition);
        }
    }
}