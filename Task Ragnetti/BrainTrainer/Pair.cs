namespace BrainTrainer_Ragnetti
{
    using System;

    public class Pair<X, Y>
    {
        private readonly X x;
        private readonly Y y;

        public Pair(X first, Y second)
        {
            this.x = first;
            this.y = second;
        }

        public X GetX()
        {
            return this.x;
        }

        public Y GetY()
        {
            return this.y;
        }

        public override string ToString()
        {
            return "<" + this.x + "," + this.y + ">";
        }
    }
}