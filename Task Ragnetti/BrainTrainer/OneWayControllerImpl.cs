namespace BrainTrainer_Ragnetti
{
    /// <summary>
    /// Implementation of OneWayController
    /// </summary>
    public class OneWayControllerImpl : IOneWayController
    {
        private readonly OneWayView view;
        private readonly OneWayModelImpl model;

        /// <summary>
        /// Initialize a new instance of the <see cref="OneWayControllerImpl" /> class.
        /// </summary>
        /// <param name="view">the view</param>
        /// <param name="difficulty">the selected difficulty</param>
        public OneWayControllerImpl(OneWayView view, DifficultyLevel difficulty)
        {
            this.view = view;
            this.model = new OneWayModelImpl(difficulty);
        }

        /// <inheritdoc />
        public void ResetGame()
        {
            this.model.OneWayInit();
            this.view.UpdateArrows(this.model.GetSteps());
            this.view.UpdateView(this.model.GetInitialPosition());
        }

        /// <inheritdoc />
        public void FirstInit()
        {
            this.view.DrawGrid(this.model.GetGridSize());
            this.model.OneWayInit();
            this.view.DrawArrows(this.model.GetSteps());
            this.view.UpdateView(this.model.GetInitialPosition());
        }

        /// <inheritdoc />
        public void ShowSolution()
        {
            this.view.ShowCorrectAnswer(this.model.GetFinalPosition());
        }
    }
}