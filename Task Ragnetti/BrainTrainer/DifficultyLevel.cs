namespace BrainTrainer_Ragnetti
{
    /// <summary>
    ///  This enumeration represents the difficulty level of the mini-games.
    /// </summary>
    public enum DifficultyLevel
    {
        /// <summary>
        /// Easy level.
        /// </summary>
        EASY,

        /// <summary>
        /// Normal level. 
        /// </summary>
        NORMAL,

        /// <summary>
        /// Hard level. 
        /// </summary>
        HARD
    }
}