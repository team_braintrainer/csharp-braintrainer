namespace Task
{
    using System;

    /// <summary>
    /// Implementation of TrueColorController.
    /// </summary>
    public class TrueColorControllerImpl : ITrueColorController
    {
        private readonly ITrueColorModel model;
        private readonly TrueColorView view;
        private readonly DifficultyLevel difficultyLevel;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrueColorControllerImpl" /> class.
        /// </summary>
        /// <param name="trueColorFx">  the view controller </param>
        /// <param name="difficultyLevel"> the current difficulty level</param>
        public TrueColorControllerImpl(TrueColorView trueColorFx, DifficultyLevel difficultyLevel)
        {
            DifficultyConfiguration configuration = DifficultyConfiguration.GetConfiguration(difficultyLevel);
            this.model = new TrueColorModelImpl(difficultyLevel, new DifficultyStrategyImpl(configuration));
            this.difficultyLevel = difficultyLevel;
            this.view = trueColorFx;
            this.view.SetSizeBtnList(this.model.GetRandomColorMap());
        }

        /// <inheritdoc/>
        public void StartGame()
        {
            this.view.SetButtons(this.model.GetRandomColorMap());
            if (this.difficultyLevel.Equals(DifficultyLevel.HARD) && this.model.GetStatusMethod())
            {
                this.view.AlternativeSetButton(this.model.GetRandomColorMap()[StatusColor.TRUECOLOR]);
            }
        }

        /// <inheritdoc/>
        public void ResetGame()
        {
            this.model.ReloadRandomColorMap();
        }

        /// <inheritdoc/>
        public void Check(string selectedAnswer)
        {
            if (this.model.MatchingMeaningAndInk(selectedAnswer))
            {
                this.view.ShowAnswer(true);
            }
            else
            {
                this.view.ShowAnswer(false);
            }

            this.view.Next();
        }
    }
}