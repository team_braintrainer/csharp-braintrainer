using System.ComponentModel;

/// <summary> 
/// Enumeration for possible color.
/// </summary>
public enum Colors
{
    /// <summary>
    /// Black color for text button.
    /// </summary>
    [field: Description("Black")]
    BLACK,

    /// <summary>
    /// Blue color for text button.
    /// </summary>
    [field: Description("Blue")]
    BLUE,

    /// <summary>
    /// Yellow color for text button.
    /// </summary>
    [field: Description("Yellow")]
    YELLOW,

    /// <summary>
    /// Red color for text button.
    /// </summary>
    [field: Description("Red")]
    RED,

    /// <summary>
    /// Green color for text button.
    /// </summary>
    [field: Description("Green")]
    GREEN,

    /// <summary>
    /// pink color for text button.
    /// </summary>
    [field: Description("Pink")]
    PINK,

    /// <summary>
    /// Brown color for text button.
    /// </summary>
    [field: Description("Brown")]
    BROWN,

    /// <summary>
    /// Orange color for text button.
    /// </summary>
    [field: Description("Orange")]
    ORANGE
    }
