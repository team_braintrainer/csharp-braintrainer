namespace Task
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This represent the Abstract Class for random color algorithms.
    /// </summary>
    public abstract class RandomColor
    {
        private readonly Dictionary<StatusColor, List<Pair<Colors, Colors>>> randomColorMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomColor" /> class.
        /// </summary>
        public RandomColor()
        {
            this.randomColorMap = new Dictionary<StatusColor, List<Pair<Colors, Colors>>>();
        }

        /// <summary>
        /// Get the random color map. 
        /// </summary>
        /// <returns> the random color map </returns>
        public Dictionary<StatusColor, List<Pair<Colors, Colors>>> GetRandomColorMap()
        {
            Console.WriteLine("sono qui");
            this.randomColorMap.Add(StatusColor.MEANCOLOR, this.GetMeanColorList());
            this.randomColorMap.Add(StatusColor.TRUECOLOR, this.GetTrueColorList());
            return this.randomColorMap;
        }

        /// <summary>
        /// Get the random color from the list passed. 
        /// </summary>
        /// <param name="list"> the list of Colors from which get Colors </param>
        /// <returns> the random color </returns>
        public Colors GetRandomColor(List<Colors> list)
        {
            var size = list.Count;
            return (Colors)list[new Random().Next(size)];
        }

        /// <summary>
        /// Get the random color list for mean color.
        /// </summary>
        /// <returns>  the random color list </returns>
        public abstract List<Pair<Colors, Colors>> GetMeanColorList();

        /// <summary>
        /// Get the random color list for true color.
        /// </summary>
        /// <returns> the random color list </returns>
        public abstract List<Pair<Colors, Colors>> GetTrueColorList();

        /// <summary>
        /// Initialize the list color.
        /// </summary>
        public abstract void Initialize();
    }
}