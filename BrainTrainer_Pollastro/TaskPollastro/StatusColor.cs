/// <summary>
/// Enumeration for the possible uses of color.
/// </summary>
public enum StatusColor 
{
    /// <summary>
    ///  Value that identify the color to focus on meaning.
    /// </summary>
    MEANCOLOR,

    /// <summary>
    /// Value that identify the color to focus true color.
    /// </summary>
    TRUECOLOR
}
