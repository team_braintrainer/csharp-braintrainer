/// <summary>
///  This enumeration represents the difficulty of the minigames.
/// </summary>
public enum DifficultyLevel
{
    /// <summary>
    /// Represent the easy level.
    /// </summary>
    EASY,

    /// <summary>
    /// Represent the normal level.
    /// </summary>
    NORMAL,

    /// <summary>
    /// Represent the hard level.
    /// </summary>
    HARD
}
