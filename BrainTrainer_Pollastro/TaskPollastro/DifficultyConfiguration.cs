/// <summary>
/// Class that convert DifficultyLevel adding configuration fields.
/// In Java was an enum, this represents a type of solution 
/// </summary>
public sealed class DifficultyConfiguration
{
    private readonly int numColor;
    private readonly int meanButton;
    private readonly int trueButton;

    /// <summary>
    /// Initializes a new instance of the <see cref="DifficultyConfiguration" /> class.
    /// </summary>
    /// <param name="difficultyLevel"> difficulty level </param>
    /// <param name="codes"> array of parameters </param>
    public DifficultyConfiguration(DifficultyLevel difficultyLevel, int[] codes)
    {
            this.numColor = (int)codes.GetValue(0);
            this.meanButton = (int)codes.GetValue(1);
            this.trueButton = (int)codes.GetValue(2);
    }

    /// <summary>
    /// Convert the DifficultyLevel to DifficultyConfiguration.
    /// </summary>
    /// <param name="difficulty"> difficulty level </param>
    /// <returns> Difficulty Configuration </returns>
    public static DifficultyConfiguration GetConfiguration(DifficultyLevel difficulty)
        {
            switch (difficulty)
            {
                case DifficultyLevel.EASY:
                    return new DifficultyConfiguration(DifficultyLevel.EASY, new[] { 4, 1, 1 });
                case DifficultyLevel.NORMAL:
                    return new DifficultyConfiguration(DifficultyLevel.NORMAL, new[] { 6, 1, 2 });
                case DifficultyLevel.HARD:
                    return new DifficultyConfiguration(DifficultyLevel.HARD, new[] { 8, 1, 2 });
                default:
                    return new DifficultyConfiguration(DifficultyLevel.EASY, new[] { 6, 1, 2 });
            }
        }

    /// <summary>
    /// Get the number of color related to difficulty
    /// </summary>
    /// <returns> number of color </returns>
    public int GetNumColor()
        {
            return this.numColor;
        }

    /// <summary>
    /// Get the number of meaning color
    /// </summary>
    /// <returns> number of meaning color</returns>
    public int GetMeanButton()
        {
            return this.meanButton;
        }

    /// <summary>
    ///  Get the number of true color
    /// </summary>
    /// <returns> number of true color</returns>
    public int GetTrueButton()
        {
            return this.trueButton;
        }
}