namespace Task
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Implementation of TrueColorModel.
    /// </summary>
    public class TrueColorModelImpl : ITrueColorModel 
    {
        private static readonly string PosAnswer = "YES";
        private readonly IDifficultyStrategy difficultySpecific;
        private Dictionary<StatusColor, List<Pair<Colors, Colors>>> randomColorMap = new Dictionary<StatusColor, List<Pair<Colors, Colors>>>();
        private bool method = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrueColorModelImpl" /> class.
        /// </summary>
        /// <param name="difficultyLevel"> the difficulty selected </param>
        /// <param name="difficultyStrategy"> the difficulty strategy create </param>
        public TrueColorModelImpl(DifficultyLevel difficultyLevel, IDifficultyStrategy difficultyStrategy) 
        {
            this.difficultySpecific = difficultyStrategy;
            ////this.difficultySpecific.InitMap();
            this.randomColorMap = this.difficultySpecific.GetRandomColorMap();
        }

        /// <inheritdoc/>
        public void ReloadRandomColorMap() 
        {
            this.AlternateMethod();
            this.randomColorMap.Clear();
            this.difficultySpecific.InitMap();
            this.randomColorMap = this.difficultySpecific.GetRandomColorMap();
        }

        /// <inheritdoc/>
        public bool MatchingMeaningAndInk(string selectedAnswer) 
        {
            int count = 0;
            foreach (Pair<Colors, Colors> mean in this.randomColorMap[StatusColor.MEANCOLOR])
            {
                foreach (Pair<Colors, Colors> trueC in this.randomColorMap[StatusColor.TRUECOLOR])
                {
                    if (trueC.GetY().Equals(mean.GetX()))
                    {
                        count++;
                    }
                }
            }

            bool match = count > 0; 
            bool answer = selectedAnswer.Equals(PosAnswer);
            return (match && answer) || (!match && !answer);
        }

        /// <inheritdoc/>
        public Dictionary<StatusColor, List<Pair<Colors, Colors>>> GetRandomColorMap() 
        {
            return this.randomColorMap;
        }

        /// <inheritdoc/>
        public bool GetStatusMethod() 
        {
            return this.method;
        }

        private void AlternateMethod()
        {
            Random random = new Random();
            this.method = random.NextDouble() >= 0.5;
        }
    }
}