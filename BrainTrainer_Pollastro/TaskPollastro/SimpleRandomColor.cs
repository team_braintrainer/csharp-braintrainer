namespace Task
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The simple random color algorithms.
    /// </summary>
    public class SimpleRandomColor : RandomColor
    {
        private readonly DifficultyConfiguration difficultyLevel;
        private readonly Random color = new Random(DateTime.Now.Millisecond);
        private List<Colors> colorList = new List<Colors>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleRandomColor" /> class.
        /// </summary>
        /// <param name="difficulty"> the difficulty configuration related difficulty level selected </param>
        public SimpleRandomColor(DifficultyConfiguration difficulty)
        {
            this.difficultyLevel = difficulty;
            this.CreateListColorDifficulty(difficulty.GetNumColor());
        }

        /// <inheritdoc/>
        public override List<Pair<Colors, Colors>> GetMeanColorList()
        {
            return this.CreateRandomColorList(this.difficultyLevel.GetMeanButton());
        }

        /// <inheritdoc/>
        public override List<Pair<Colors, Colors>> GetTrueColorList()
        {
            return this.CreateRandomColorList(this.difficultyLevel.GetTrueButton());
        }

        /// <inheritdoc/>
        public override void Initialize()
        {
            throw new System.NotImplementedException();
        }

        private void CreateListColorDifficulty(int numberColor)
        {
            for (int i = 0; i < numberColor; i++)
            {
                this.colorList.Add((Colors)i);
            }
        }

        private List<Pair<Colors, Colors>> CreateRandomColorList(int numberBtn)
        {
            List<Pair<Colors, Colors>> list = new List<Pair<Colors, Colors>>();
            for (int i = 0; i < numberBtn; i++)
            {
                list.Add(new Pair<Colors, Colors>(this.GetRandomColor(), this.GetRandomColor()));
            }

            return list;
        }

        private Colors GetRandomColor()
        {
            Colors color = this.colorList[this.color.Next(this.colorList.Count)];
            return color;
        }
    }
}