namespace Task
{
    using System;

    /// <summary>
    /// Class that implements a Pair composed by two generic Objects.
    /// </summary>
    /// <typeparam name="X"> first element  </typeparam>
    /// <typeparam name="Y"> second element </typeparam>
    public class Pair<X, Y>
    {
        private readonly X x;
        private readonly Y y;

        /// <summary>
        /// Initializes a new instance of the <see cref="Pair{X, Y}" /> class.
        /// </summary>
        /// <param name="x"> first element </param>
        /// <param name="y"> second element </param>
        public Pair(X x, Y y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Get the x element.
        /// </summary>
        /// <returns> the x element </returns>
        public X GetX()
        {
            return this.x;
        }

        /// <summary>
        /// Get the y element.
        /// </summary>
        /// <returns> the y element </returns>
        public Y GetY()
        {
            return this.y;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = (prime * result) + ((this.x == null) ? 0 : this.x.GetHashCode());
            result = (prime * result) + ((this.y == null) ? 0 : this.y.GetHashCode());
            return result;
        }

        /// <summary>
        /// Compare objects
        /// </summary>
        /// <param name="obj"> object to compare </param>
        /// <returns> true if the objects are equal </returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            if (obj == null)
            {
                return false;
            }

            if (GetType() != obj.GetType())
            {
                return false;
            }

            Pair<X, Y> other = (Pair<X, Y>)obj;
            if (this.x == null)
            {
                if (other.x != null)
                {
                    return false;
                }
            }
            else if (!this.x.Equals(other.x))
            {
                return false;
            }

            if (this.y == null)
            {
                if (other.y != null)
                {
                    return false;
                }
            }
            else if (!this.y.Equals(other.y))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Pair to string
        /// </summary>
        /// <returns> string of pair  </returns>
        public override string ToString()
        {
            return "Pair [x=" + this.x + ", y=" + this.y + "]";
        }
    }
}