namespace Task
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Implementation of DifficultyStrategy.
    /// </summary>
    public class DifficultyStrategyImpl : IDifficultyStrategy
    {
        private static readonly int NUMCOLOR = 4;
        private readonly Dictionary<StatusColor, List<Pair<Colors, Colors>>> randomColorMap;
        private RandomColor colorRandom;

        /// <summary>
        /// Initializes a new instance of the <see cref="DifficultyStrategyImpl" /> class.
        /// </summary>
        /// <param name="difficulty"> is a difficulty configuration.</param>
        public DifficultyStrategyImpl(DifficultyConfiguration difficulty)
        {
            this.randomColorMap = new Dictionary<StatusColor, List<Pair<Colors, Colors>>>();
            if (difficulty.GetNumColor() >= NUMCOLOR)
            {
                this.colorRandom = new SimpleRandomColor(difficulty);
                this.CreateRandomColorMap();
            }
        }

        /// <inheritdoc/>
        public void InitMap()
        {
            this.randomColorMap.Clear();
            this.CreateRandomColorMap();
        }

        /// <inheritdoc/>
        public Dictionary<StatusColor, List<Pair<Colors, Colors>>> GetRandomColorMap()
        {
            return this.randomColorMap;
        }

        private void CreateRandomColorMap()
        {
            this.randomColorMap.Add(StatusColor.MEANCOLOR, this.colorRandom.GetMeanColorList());
            this.randomColorMap.Add(StatusColor.TRUECOLOR, this.colorRandom.GetTrueColorList());
        }
    }
}