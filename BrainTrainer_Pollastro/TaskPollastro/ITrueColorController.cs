namespace Task
{
    using System;

    /// <summary>
    /// A interface controller for the game True Color.
    /// </summary>
    public interface ITrueColorController
    {
        /// <summary>
        /// Builds and transfer the useful elements to the View.
        /// </summary>
        void StartGame();

        /// <summary>
        /// Check if is the right answer.
        /// </summary>
        /// <param name="selectedAnswer"> answer to check </param>
        void Check(string selectedAnswer);

        /// <summary>
        ///  Reset components for next scene
        /// </summary>
        void ResetGame();
    }
}