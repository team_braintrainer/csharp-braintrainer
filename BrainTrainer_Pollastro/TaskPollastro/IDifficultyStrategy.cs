namespace Task
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface DifficultyStrategy.
    /// </summary>
    public interface IDifficultyStrategy
    {
        /// <summary>
        /// Initialize the random color map.
        /// </summary>
        void InitMap();

        /// <summary>
        /// Getter for the random color map.
        /// </summary>
        /// <returns>  the created random color map </returns>
        Dictionary<StatusColor, List<Pair<Colors, Colors>>> GetRandomColorMap();
    }
}