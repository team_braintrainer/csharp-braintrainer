﻿namespace Task
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// True Color View 
    /// </summary>
    public partial class TrueColorView : Form
    {
        private static readonly int FontSize = 13;
        private int meanColorSize;
        private int trueColorSize;
        private List<Button> meaningColorBtnList;
        private List<Button> trueColorBtnList;
        private ITrueColorController trueColorController;
        private int count = 0;
        private int pos = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrueColorView" /> class.
        /// </summary>
        public TrueColorView()
        {
            this.InitializeComponent();
            this.AddAnswerButton();
        }

        /// <summary>
        /// Reload the components of the scene.
        /// </summary>
        public void Next()
        {
            this.trueColorController.ResetGame();
            this.trueColorController.StartGame();
        }

        /// <summary>
        /// Initialize the useful parameters
        /// </summary>
        /// <param name="difficultyLevel"> the difficulty selected </param>
        public void InitGame(DifficultyLevel difficultyLevel)
        {
            this.trueColorController = new TrueColorControllerImpl(this, difficultyLevel);
            this.PrepareButtons();
            this.trueColorController.StartGame();
        }

        /// <summary>
        /// Set the number of button.
        /// </summary>
        /// <param name="colorMap"> map with lists of Colors pair</param>
        public void SetSizeBtnList(Dictionary<StatusColor, List<Pair<Colors, Colors>>> colorMap)
        {
            this.meanColorSize = colorMap[StatusColor.MEANCOLOR].Count;
            this.trueColorSize = colorMap[StatusColor.TRUECOLOR].Count;
        }

        /// <summary>
        /// Set a random color and a random textColor to buttons.
        /// </summary>
        /// <param name="colorMap"> map with lists of Colors pair </param>
        public void SetButtons(Dictionary<StatusColor, List<Pair<Colors, Colors>>> colorMap)
        {
            this.SetStyleButton(colorMap[StatusColor.MEANCOLOR], this.meaningColorBtnList);
            this.SetStyleButton(colorMap[StatusColor.TRUECOLOR], this.trueColorBtnList);
        }

        /// <summary>
        /// Set a random color to the buttons background.
        /// </summary>
        /// <param name="lists"> list of random color belonging to true color list</param>
        public void AlternativeSetButton(List<Pair<Colors, Colors>> lists)
        {
            var size = this.trueColorBtnList.Count;
            for (int i = 0; i < size; i++)
            {
                this.trueColorBtnList[i].Text = string.Empty;
                this.trueColorBtnList[i].BackColor = Color.FromName(lists[i].GetY().ToString());
            }
        }

        /// <summary>
        /// Show the answer coloring the button's border.
        /// </summary>
        /// <param name="ans"> identify if the answer is correct or not.</param>
        public void ShowAnswer(bool ans)
        {
            if (ans)
            {
                MessageBox.Show("Corretto");
            }
            else
            {
                MessageBox.Show("Sbagliato");
            }
        }

        private void ButtonClicked(object sender, EventArgs e)
        {
            Button srcButton = (Button)sender;
            string selectedAnswer = srcButton.Text;
            this.trueColorController.Check(selectedAnswer);
        }

        private void AddAnswerButton()
        {
            Button yesBtn = new Button();
            yesBtn.Size = new Size(100, 100);
            yesBtn.Location = new Point(300, 300);
            yesBtn.Text = "YES";
            yesBtn.Font = new Font(yesBtn.Font.FontFamily, FontSize);
            this.Controls.Add(yesBtn);
            yesBtn.Click += new EventHandler(this.ButtonClicked);

            Button noBtn = new Button();
            noBtn.Size = new Size(100, 100);
            noBtn.Location = new Point(150, 300);
            noBtn.Text = "NO";
            noBtn.Font = new Font(noBtn.Font.FontFamily, FontSize);
            this.Controls.Add(noBtn);
            noBtn.Click += new EventHandler(this.ButtonClicked);
        }

        private void AddButton(List<Button> btnList, int size)
        {
            for (int i = 0; i < size; i++)
            {
                if ( this.count >= 1)
                {
                    this.pos = this.count * 100;
                }

                this.count = this.count + 1;
                Button button = new Button();
                button.Size = new Size(100, 100);
                button.Location = new Point(225, this.pos);
                button.Font = new Font(button.Font.FontFamily, FontSize);
                btnList.Add(button);
                this.Controls.Add(button);
            }
        }

        private void PrepareButtons()
        {
            this.meaningColorBtnList = new List<Button>();
            this.trueColorBtnList = new List<Button>();
            this.AddButton(this.meaningColorBtnList, this.meanColorSize);
            this.AddButton(this.trueColorBtnList, this.trueColorSize);
        }

        private void SetStyleButton(List<Pair<Colors, Colors>> listColor, List<Button> listBtn)
        {
            int size = listBtn.Count;
            for (int i = 0; i < size; i++)
            {
                listBtn[i].Text = listColor[i].GetX().ToString();
                listBtn[i].ForeColor = Color.FromName(listColor[i].GetY().ToString());
            }
        }
    }
}
