namespace Task
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The model interface of TrueColor minigame.
    /// </summary>
    public interface ITrueColorModel
    {
        /// <summary>
        /// Reload the map containing the random color for the button.
        /// </summary>
        void ReloadRandomColorMap();

        /// <summary>
        /// Check if is the right answer.
        /// </summary>
        /// <param name="selectedAnswer"> answer to check </param>
        /// <returns>  true if the meaning and the ink match </returns>
        bool MatchingMeaningAndInk(string selectedAnswer);

        /// <summary>
        /// Get the a Map of random color.
        /// </summary>
        /// <returns> map of random color</returns>
        Dictionary<StatusColor, List<Pair<Colors, Colors>>> GetRandomColorMap();

        /// <summary>
        ///  Get the a boolean in order to call a specific method.
        /// </summary>
        /// <returns> true call different method </returns>
        bool GetStatusMethod();
    }
}