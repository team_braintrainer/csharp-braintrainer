﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents the perilous path difficulty builder.
    /// </summary>
    public interface IPerilousPathDifficultyBuilder : IDifficultyBuilder<IPerilousPathDifficultyBuilder>
    {
        /// <summary>
        /// Modify the default number of mines in a specific difficulty.
        /// </summary>
        /// <param name="numMines">the amount of mines</param>
        /// <returns><see cref="IPerilousPathDifficultyBuilder">which is the specific difficulty builder</see></returns>
        IPerilousPathDifficultyBuilder SetNumMines(int numMines);

        /// <summary>
        /// Build the perilous path difficulty.
        /// </summary>
        /// <returns><see cref="IPerilousPathDifficulty">which is the game's difficulty</see></returns>
        IPerilousPathDifficulty Build();
    }
}
