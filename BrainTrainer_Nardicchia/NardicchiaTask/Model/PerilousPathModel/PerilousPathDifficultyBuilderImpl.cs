﻿using System;
using System.Collections.Generic;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Implementation of <see cref="IPerilousPathDifficultyBuilder">the specific difficulty builder of the game</see>.
    /// </summary>
    public class PerilousPathDifficultyBuilderImpl : IPerilousPathDifficultyBuilder
    {
        /// <summary>
        /// The size's lower bound.
        /// </summary>
        private const int MINIMUMSIZE = 4;

        /// <summary>
        /// The initial size.
        /// </summary>
        private const int SIZE = 5;

        /// <summary>
        /// The initial amount of mines.
        /// </summary>
        private const int NUMMINES = 10;

        private IDictionary<DifficultyLevel, KeyValuePair<int, int>> difficulty;
        private DifficultyLevel? currentDifficultyLevel;
        private int? size;
        private int? numMines;
        private bool built;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerilousPathDifficultyBuilderImpl"/> class.
        /// </summary>
        public PerilousPathDifficultyBuilderImpl()
        {
            this.difficulty = this.InitDefaultDifficulty();
            this.currentDifficultyLevel = null;
            this.size = null;
            this.numMines = null;
            this.built = false;
        }

        /// <inheritdoc cref="IDifficultyBuilder{X}"/>
        public IPerilousPathDifficultyBuilder SetDifficultyLevel(DifficultyLevel currentDifficultyLevel)
        {
            Check(!this.built);
            this.currentDifficultyLevel = ReassignNullable(this.currentDifficultyLevel.HasValue, currentDifficultyLevel);
            return this;
        }

        /// <inheritdoc cref="IDifficultyBuilder{X}"/>
        public IPerilousPathDifficultyBuilder SetSize(int size)
        {
            Check(!this.built);
            this.size = ReassignNullable(this.size.HasValue, size);
            Validity(size < MINIMUMSIZE);
            int numMines = this.difficulty[this.currentDifficultyLevel.GetValueOrDefault()].Value;
            this.difficulty[this.currentDifficultyLevel.GetValueOrDefault()] = new KeyValuePair<int, int>(size, numMines);
            return this;
        }

        /// <inheritdoc cref="IPerilousPathDifficultyBuilder"/>
        public IPerilousPathDifficultyBuilder SetNumMines(int numMines)
        {
            Check(!this.built);
            this.numMines = ReassignNullable(this.numMines.HasValue, numMines);
            int size = this.difficulty[this.currentDifficultyLevel.GetValueOrDefault()].Key;
            Validity(numMines >= size * size);
            this.difficulty[this.currentDifficultyLevel.GetValueOrDefault()] = new KeyValuePair<int, int>(size, numMines);
            return this;
        }

        /// <inheritdoc cref="IPerilousPathDifficultyBuilder"/>
        public IPerilousPathDifficulty Build()
        {
            Check(!this.built);
            Check(this.currentDifficultyLevel.HasValue);
            this.built = true;
            return new PerilousPathDifficultyImpl(this.difficulty, this.currentDifficultyLevel);
        }

        private static void Check(bool b)
        {
            if (!b)
            {
                throw new InvalidOperationException();
            } 
        }

        private static void Validity(bool b)
        {
            if (b)
            {
                throw new ArgumentException();
            }
        }

        private static X ReassignNullable<X>(bool b, X x)
        {
            Check(!b);
            return x;
        }

        private int Increment(DifficultyLevel difficultyLevel)
        {
            return (int)difficultyLevel;
        }

        private IDictionary<DifficultyLevel, KeyValuePair<int, int>> InitDefaultDifficulty()
        {
            this.difficulty = new Dictionary<DifficultyLevel, KeyValuePair<int, int>>();
            Array difficultyNames = Enum.GetNames(typeof(DifficultyLevel));
            foreach (string difficultyName in difficultyNames)
            {
                DifficultyLevel d = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel), difficultyName);
                this.difficulty.Add(d, new KeyValuePair<int, int>(SIZE + this.Increment(d), (NUMMINES * this.Increment(d)) + NUMMINES));
            }

            return this.difficulty;
        }

        /// <summary>
        /// Implementation of <see cref="IPerilousPathDifficulty">the perilous path difficulty</see>.
        /// </summary>
        internal class PerilousPathDifficultyImpl : IPerilousPathDifficulty
        {
            private DifficultyLevel? optCurrentDifficultyLevel;
            private IDictionary<DifficultyLevel, KeyValuePair<int, int>> difficulty;

            /// <summary>
            /// Initializes a new instance of the <see cref="PerilousPathDifficultyImpl"/> class.
            /// </summary>
            /// <param name="difficulty">the values corresponding to the current difficulty</param>
            /// <param name="optCurrentDifficultyLevel">the current difficulty</param>
            public PerilousPathDifficultyImpl(IDictionary<DifficultyLevel, KeyValuePair<int, int>> difficulty, DifficultyLevel? optCurrentDifficultyLevel)
            {
                this.difficulty = difficulty;
                this.optCurrentDifficultyLevel = optCurrentDifficultyLevel;
            }

            /// <inheritdoc cref="IPerilousPathDifficulty"/>
            public int GetSize()
            {
                return this.GetValue().Key;
            }

            /// <inheritdoc cref="IPerilousPathDifficulty"/>
            public int GetNumMines()
            {
                return this.GetValue().Value;
            }

            private KeyValuePair<int, int> GetValue()
            {
                return this.difficulty[this.optCurrentDifficultyLevel.GetValueOrDefault()];
            }
        }
    }
}