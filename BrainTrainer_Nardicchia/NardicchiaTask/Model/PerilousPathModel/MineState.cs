﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents the state of a mine.
    /// </summary>
    public enum MineState
    {
        /// <summary>
        /// The mine is not detonated.
        /// </summary>
        Undetonated,

        /// <summary>
        /// The mine is exploded.
        /// </summary>
        Exploded
    }

    /// <summary>
    /// Extensions of the <see cref="MineState">mine state</see> enumeration class.
    /// </summary>
    public static class MineStateExtensions
    {
        /// <summary>
        /// Check if the user hits the mine.
        /// </summary>
        /// <param name="mineState">the mine state</param>
        /// <returns>true if the mine is exploded, false otherwise</returns>
        public static bool IsExploded(this MineState mineState)
        {
            return mineState == MineState.Undetonated ? false : true;
        }
    }
}
