﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents the game's model.
    /// </summary>
    public interface IPerilousPathModel
    {
        /// <summary>
        /// Check if the user can hit this position.
        /// </summary>
        /// <param name="row">the step's position row</param>
        /// <param name="col">the step's position col</param>
        /// <returns>true if user can hit this coordinate, false otherwise</returns>
        bool Hit(int row, int col);

        /// <summary>
        /// Gets the size of this current level.
        /// </summary>
        /// <returns>the size</returns>
        int GetSize();

        /// <summary>
        /// Gets the start position.
        /// </summary>
        /// <returns>the initial step</returns>
        KeyValuePair<int, int> GetStart();

        /// <summary>
        /// Gets the finish position.
        /// </summary>
        /// <returns>the end step</returns>
        KeyValuePair<int, int> GetFinish();
       
        /// <summary>
        /// Gets the mines.
        /// </summary>
        /// <returns>the mines</returns>
        ISet<IMine> GetMines();

        /// <summary>
        /// Check if the user finds a path correctly.
        /// </summary>
        /// <returns>true if the user hasn't made a mistake, false otherwise</returns>
        bool IsDone();

        /// <summary>
        /// Check if the user fails.
        /// </summary>
        /// <returns>true if the user has made a mistake, false otherwise</returns>
        bool IsFailed();

        /// <summary>
        /// Reset the game at the initial state.
        /// </summary>
        void ResetGame();
    }
}
