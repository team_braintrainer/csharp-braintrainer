﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Implementation of a simple <see cref="IMine">mine</see>.
    /// </summary>
    public class SimpleMineImpl : AbstractMine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleMineImpl"/> class.
        /// </summary>
        /// <param name="size">the upper bound's size</param>
        public SimpleMineImpl(int size) : base(size)
        {
        }
    }
}
