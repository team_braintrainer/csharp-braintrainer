﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Implementation of <see cref="IPerilousPathModel">the game's model</see>.
    /// </summary>
    public class PerilousPathModelImpl : IPerilousPathModel
    {
        private int size;
        private int numMines;
        private IMineFactory mineFactory;
        private LinkedList<KeyValuePair<int, int>> selected;
        private KeyValuePair<int, int> start;
        private KeyValuePair<int, int> finish;
        private ISet<IMine> mines;
        private LinkedList<KeyValuePair<int, int>> path;

        /// <summary>
        /// Initializes a new instance of the <see cref="PerilousPathModelImpl"/> class.
        /// </summary>
        /// <param name="difficultyLevel">the current difficulty level</param>
        public PerilousPathModelImpl(DifficultyLevel difficultyLevel)
        {
            IPerilousPathDifficulty difficulty = new PerilousPathDifficultyBuilderImpl()
                                                    .SetDifficultyLevel(difficultyLevel)
                                                    .Build();
            this.selected = new LinkedList<KeyValuePair<int, int>>();
            this.path = new LinkedList<KeyValuePair<int, int>>();
            this.size = difficulty.GetSize();
            Check(this.size);
            this.numMines = difficulty.GetNumMines();
            Check(this.numMines);
            this.mineFactory = new MineFactoryImpl();
        }

        public bool Hit(int row, int col)
        {
            if (this.CouldHit(row, col))
            {
                IEnumerable<IMine> filterMines = this.mines.Where(m => (m.GetMinePosition().Key == row && m.GetMinePosition().Value == col));
                if (filterMines != null)
                {
                    foreach (IMine mine in filterMines)
                    {
                        mine.SetExploded();
                    }
                }

                this.selected.AddLast(new KeyValuePair<int, int>(row, col));
                return true;
            }

            return false;
        }

        public int GetSize()
        {
            return this.size;
        }

        public KeyValuePair<int, int> GetStart()
        {
            return this.start;
        }

        public KeyValuePair<int, int> GetFinish()
        {
            return this.finish;
        }

        public ISet<IMine> GetMines()
        {
            return this.mines;
        }

        public bool IsDone()
        {
            return this.selected.Count != 0 && this.selected.Last().Equals(this.finish);
        }

        public bool IsFailed()
        {
            return this.mines.Any(m => m.IsExploded());
        }

        public void ResetGame()
        {
            this.InitPath();
            this.InitMines();
            this.selected.Clear();
        }

        private static void Check(int value)
        {
            Predicate<int> predicate = (i) => i == 0;
            if (predicate.Invoke(value))
            {
                throw new InvalidOperationException();
            }
        }

        private IMine GetRandomMines()
        {
            IMine mine = this.mineFactory.CreateSimpleMine(this.size);
            KeyValuePair<int, int> minePosition = mine.GetMinePosition();
            return this.path.Contains(minePosition) || this.start.Equals(minePosition) || this.finish.Equals(minePosition)
                        || this.mines.Any(m => m.GetMinePosition().Equals(minePosition)) ? this.GetRandomMines() : mine;
        }

        private bool CouldMove(KeyValuePair<int, int> position, KeyValuePair<int, int> last)
        {
            return Math.Abs(position.Key - last.Key) + Math.Abs(position.Value - last.Value) == 1;
        }

        private bool CouldHit(int row, int col)
        {
            KeyValuePair<int, int> position = new KeyValuePair<int, int>(row, col);
            return (this.selected.Count == 0 && position.Equals(this.start)) || (!this.selected.Contains(position)
                && this.selected.Count != 0 && this.CouldMove(position, this.selected.Last()));
        }

        private void InitPath()
        {
            this.path = PathGameImpl.CreateRandomDirectionPath(this.size, this.numMines).GetPathList();
            this.start = this.path.First();
            this.finish = this.path.Last();
        }

        private void InitMines()
        {
            this.mines = new HashSet<IMine>();
            for (int i = 0; i < this.numMines; i++)
            {
                this.mines.Add(this.GetRandomMines());
            }
        }
    }
}
