﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents the possible directions.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Left direction.
        /// </summary>
        Left,

        /// <summary>
        /// Right direction.
        /// </summary>
        Right,

        /// <summary>
        /// Up direction.
        /// </summary>
        Up,

        /// <summary>
        /// Down direction.
        /// </summary>
        Down
    }

    /// <summary>
    /// Extensions of the <see cref="Direction">direction</see> enumeration class.
    /// </summary>
    public static class DirectionExtensions
    {
        /// <summary>
        /// Horizontal direction
        /// </summary>
        /// <param name="horizontalDirection">the horizontal direction</param>
        /// <returns>1 to move rightward, -1 to move leftward</returns>
        public static int Horizontal(this Direction horizontalDirection)
        {
            return horizontalDirection == Direction.Right ? 1 : -1;
        }

        /// <summary>
        /// Vertical direction.
        /// </summary>
        /// <param name="verticalDirection">the vertical direction</param>
        /// <returns>1 to move downward, -1 to move upward</returns>
        public static int Vertical(this Direction verticalDirection)
        {
            return verticalDirection == Direction.Down ? 1 : -1;
        }
    }
}
