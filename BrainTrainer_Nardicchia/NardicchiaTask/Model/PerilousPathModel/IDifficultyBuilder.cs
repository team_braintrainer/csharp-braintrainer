﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents the generic difficulty builder.
    /// </summary>
    /// <typeparam name="X">the specific difficulty builder</typeparam>
    public interface IDifficultyBuilder<X>
    {
        /// <summary>
        /// Initialize the current difficulty level.
        /// </summary>
        /// <param name="currentDifficultyLevel">the current difficulty level</param>
        /// <returns>a generic difficulty builder</returns>
        X SetDifficultyLevel(DifficultyLevel currentDifficultyLevel);

        /// <summary>
        /// Modify the default size in a specific difficulty.
        /// </summary>
        /// <param name="size">the dimension's size</param>
        /// <returns>a generic difficulty builder</returns>
        X SetSize(int size);
    }
}
