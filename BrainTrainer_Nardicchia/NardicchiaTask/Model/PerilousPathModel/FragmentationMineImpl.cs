﻿using System;
using System.Collections.Generic;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents a <see cref="IFragmentationMine">fragmentation mine</see>.
    /// </summary>
    public class FragmentationMineImpl : AbstractMine, IFragmentationMine, IMine
    {
        private int size;

        /// <summary>
        /// Initializes a new instance of the <see cref="FragmentationMineImpl"/> class.
        /// </summary>
        /// <param name="size">the upper bound's size</param>
        public FragmentationMineImpl(int size) : base(size)
        {
            this.size = size;
        }

        /// <inheritdoc cref="IFragmentationMine"/>
        public ISet<KeyValuePair<int, int>> GetScraps()
        {
            ISet<KeyValuePair<int, int>> scraps = new HashSet<KeyValuePair<int, int>>();
            if (this.IsExploded())
            {
                IList<KeyValuePair<int, int>> tempScraps = new List<KeyValuePair<int, int>>();
                KeyValuePair<int, int> mine = this.GetMinePosition();
                tempScraps.Add(new KeyValuePair<int, int>(mine.Key + Direction.Left.Horizontal(), mine.Value + Direction.Up.Vertical()));
                tempScraps.Add(new KeyValuePair<int, int>(mine.Key + Direction.Left.Horizontal(), mine.Value + Direction.Down.Vertical()));
                tempScraps.Add(new KeyValuePair<int, int>(mine.Key + Direction.Right.Horizontal(), mine.Value + Direction.Up.Vertical()));
                tempScraps.Add(new KeyValuePair<int, int>(mine.Key + Direction.Right.Horizontal(), mine.Value + Direction.Down.Vertical()));

                foreach (KeyValuePair<int, int> s in tempScraps)
                {
                    if (!this.CheckReachLimit(s))
                    {
                        scraps.Add(s);
                    }
                }
            }

            return scraps;
        }

        private bool CheckReachLimit(KeyValuePair<int, int> lastPosition)
        {
            return lastPosition.Key < 0 || lastPosition.Value < 0 || lastPosition.Key >= this.size || lastPosition.Value >= this.size;
        }
    }
}