﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents the factory of various type of <see cref="IMine">mine</see>.
    /// </summary>
    public interface IMineFactory
    {
        /// <summary>
        /// Create a simple <see cref="IMine">mine</see>.
        /// </summary>
        /// <param name="size">the dimension's size</param>
        /// <returns>the <see cref="IMine">mine</see></returns>
        IMine CreateSimpleMine(int size);

        /// <summary>
        /// Create a <see cref="IFragmentationMine">fragmentation mine</see>.
        /// </summary>
        /// <param name="size">the dimension's size</param>
        /// <returns>the <see cref="IFragmentationMine">fragmentation mine</see></returns>
        IFragmentationMine CreateFragmentationMine(int size);
    }
}
