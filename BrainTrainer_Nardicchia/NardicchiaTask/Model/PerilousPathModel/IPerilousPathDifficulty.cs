﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents the difficulty at each level.
    /// </summary>
    public interface IPerilousPathDifficulty
    {
        /// <summary>
        /// Specific size of the current difficulty.
        /// </summary>
        /// <returns>the size</returns>
        int GetSize();

        /// <summary>
        /// Specific amount of mines of the current difficulty.
        /// </summary>
        /// <returns>the number of mines</returns>
        int GetNumMines();
    }
}
