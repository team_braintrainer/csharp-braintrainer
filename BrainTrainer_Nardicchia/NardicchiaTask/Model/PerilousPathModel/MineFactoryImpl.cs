﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Implementation of <see cref="IMineFactory">mine factory</see>.
    /// </summary>
    public class MineFactoryImpl : IMineFactory
    {
        /// <inheritdoc cref="IMineFactory"/>
        public IMine CreateSimpleMine(int size)
        {
            return new SimpleMineImpl(size);
        }

        /// <inheritdoc cref="IMineFactory"/>
        public IFragmentationMine CreateFragmentationMine(int size)
        {
            return new FragmentationMineImpl(size);
        }
    }
}
