﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{ 

    /// <summary>
    /// Represents the game's path.
    /// </summary>
    public interface IPathGame
    {
        /// <summary>
        /// Gets the game's path.
        /// </summary>
        /// <returns>the game's path</returns>
        LinkedList<KeyValuePair<int, int>> GetPathList();
    }
}
