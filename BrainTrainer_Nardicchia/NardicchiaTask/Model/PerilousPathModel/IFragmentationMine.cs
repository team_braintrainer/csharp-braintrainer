﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Implementation of a <see cref="IFragmentationMine">fragmentation mine</see>.
    /// </summary>
    public interface IFragmentationMine : IMine
    {
        /// <summary>
        /// Gets the fragments position.
        /// </summary>
        /// <returns>a set of positions of the scraps if the mine is exploded</returns>
        ISet<KeyValuePair<int, int>> GetScraps();
    }
}