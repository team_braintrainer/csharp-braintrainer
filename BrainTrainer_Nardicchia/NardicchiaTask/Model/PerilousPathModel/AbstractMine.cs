﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// This abstract class implements the common aspect of <see cref="IMine">mine</see>.
    /// </summary>
    public abstract class AbstractMine : IMine
    {
        private KeyValuePair<int, int> mine;
        private MineState mineState;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractMine"/> class.
        /// </summary>
        /// <param name="size">the upper bound's size</param>
        public AbstractMine(int size)
        {
            Random random = new Random();
            this.mine = new KeyValuePair<int, int>(random.Next(size), random.Next(size));
            this.mineState = MineState.Undetonated;
        }

        /// <inheritdoc cref="IMine"/>
        public KeyValuePair<int, int> GetMinePosition()
        {
            return this.mine;
        }

        /// <inheritdoc cref="IMine"/>
        public void SetExploded()
        {
            this.mineState = MineState.Exploded;
        }

        /// <inheritdoc cref="IMine"/>
        public bool IsExploded()
        {
            return this.mineState.IsExploded();
        }
    }
}
