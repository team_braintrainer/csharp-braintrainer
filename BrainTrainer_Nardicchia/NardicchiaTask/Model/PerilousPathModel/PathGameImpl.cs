﻿using System;
using System.Collections.Generic;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Implementations of <see cref="IPathGame">the game's path</see>.
    /// </summary>
    public class PathGameImpl : IPathGame
    {
        private int size;
        private int numMines;
        private KeyValuePair<int, int> start;
        private KeyValuePair<int, int> finish;
        private LinkedList<KeyValuePair<int, int>> path;

        /// <summary>
        /// Prevents a default instance of the <see cref="PathGameImpl"/> class from being created.
        /// </summary>
        private PathGameImpl()
        {
        }

        /// <summary>
        /// Create a path based on the random direction.
        /// </summary>
        /// <param name="size">the size of the current difficulty level</param>
        /// <param name="numMines">the amount of mines of the current difficulty level</param>
        /// <returns><see cref="IPathGame">the game's path</see></returns>
        public static IPathGame CreateRandomDirectionPath(int size, int numMines)
        {
            Check(size);
            Check(numMines);
            KeyValuePair<int, int> start = InitStart(size);
            KeyValuePair<int, int> finish = InitFinish(size, start);
            return PathGameImpl.CreateGenericRandomDirectionPath(size, numMines, start, finish);
        }

        /// <summary>
        /// Create a path based on the random direction with default initial step.
        /// </summary>
        /// <param name="size">the size of the current difficulty level</param>
        /// <param name="numMines">the amount of mines of the current difficulty level</param>
        /// <returns><see cref="IPathGame">the game's path</see></returns>
        public static IPathGame CreateRandomDirectionPathDefaultStart(int size, int numMines)
        {
            Check(size);
            Check(numMines);
            KeyValuePair<int, int> start = new KeyValuePair<int, int>(0, 0);
            KeyValuePair<int, int> finish = InitFinish(size, start);
            return PathGameImpl.CreateGenericRandomDirectionPath(size, numMines, start, finish);
        }

        /// <inheritdoc cref="IPathGame"/>
        public LinkedList<KeyValuePair<int, int>> GetPathList()
        {
            return this.path;
        }

        private static IPathGame CreateGenericRandomDirectionPath(int size, int numMines, KeyValuePair<int, int> start, KeyValuePair<int, int> finish)
        {
            PathGameImpl pathGame = new PathGameImpl();
            pathGame.size = size;
            pathGame.numMines = numMines;
            pathGame.start = start;
            pathGame.finish = finish;
            pathGame.path = new LinkedList<KeyValuePair<int, int>>();
            pathGame.InitPath();
            return pathGame;
        }

        private static KeyValuePair<int, int> GetRandomPosition(int size)
        {
            Random random = new Random();
            return new KeyValuePair<int, int>(random.Next(size), random.Next(size));
        }

        private static bool Adjacent(KeyValuePair<int, int> position1, KeyValuePair<int, int> position2)
        {
            return Math.Abs(position1.Key - position2.Key) <= 1 && Math.Abs(position1.Value - position2.Value) <= 1;
        }

        private static KeyValuePair<int, int> InitStart(int size)
        {
            return GetRandomPosition(size);
        }

        private static KeyValuePair<int, int> InitFinish(int size, KeyValuePair<int, int> start)
        {
            KeyValuePair<int, int> finish = GetRandomPosition(size);
            if (finish.Equals(start) || Adjacent(finish, start))
            {
                return InitFinish(size, start);
            }
            else
            {
                return finish;
            }
        }

        private static void Check(int value)
        {
            Predicate<int> predicate = (i) => i == 0;
            if (predicate.Invoke(value))
            {
                throw new InvalidOperationException();
            }
        }

        private Direction GetRandomDirection()
        {
            Random random = new Random();
            Array difficultyValues = Enum.GetValues(typeof(DifficultyLevel));
            return (Direction)difficultyValues.GetValue(random.Next(difficultyValues.Length));
        }

        private bool CheckReachLimit(KeyValuePair<int, int> lastPosition)
        {
            return lastPosition.Key < 0 || lastPosition.Value < 0 || lastPosition.Key >= this.size || lastPosition.Value >= this.size;
        }

        private void SetPath()
        {
            Direction direction = this.GetRandomDirection();
            KeyValuePair<int, int> lastPosition;
            if (direction == Direction.Right || direction == Direction.Left)
            {
                lastPosition = new KeyValuePair<int, int>(this.path.Last.Value.Key, this.path.Last.Value.Value + direction.Horizontal());
            }
            else
            {
                lastPosition = new KeyValuePair<int, int>(this.path.Last.Value.Key + direction.Vertical(), this.path.Last.Value.Value);
            }

            if (!this.path.Contains(lastPosition) && !this.CheckReachLimit(lastPosition))
            {
                this.path.AddLast(lastPosition);
            }
            else
            {
                if (this.path.Count >= 2)
                {
                    this.path.RemoveLast();
                }

                this.SetPath();
            }
        }

        private void InitPath()
        {
            this.path.Clear();
            this.path.AddFirst(this.start);
            while (this.path.Last.Value.Key != this.finish.Key && this.path.Last.Value.Value != this.finish.Value)
            {
                if (this.path.Count < (this.size * this.size) - this.numMines)
                {
                    this.SetPath();
                }
                else
                {
                    this.InitPath();
                }
            }
        }
    }
}