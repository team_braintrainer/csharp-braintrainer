﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model.PerilousPathModel
{
    /// <summary>
    /// Represents a mine.
    /// </summary>
    public interface IMine
    {
        /// <summary>
        /// Gets the mine's position.
        /// </summary>
        /// <returns>the mine coordinate.</returns>
        KeyValuePair<int, int> GetMinePosition();

        /// <summary>
        /// Sets the state of this mine as exploded.
        /// </summary>
        void SetExploded();

        /// <summary>
        /// Check if the mine is exploded.
        /// </summary>
        /// <returns>true if this mine is exploded, false otherwise.</returns>
        bool IsExploded();
    }
}
