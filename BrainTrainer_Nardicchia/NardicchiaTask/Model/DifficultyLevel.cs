﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NardicchiaTask.Model
{
    /// <summary>
    /// This enumeration represents the difficulty of the mini games.
    /// </summary>
    public enum DifficultyLevel
    {
        /// <summary>
        /// Represent the easy level.
        /// </summary>
        Easy,

        /// <summary>
        /// Represent the normal level.
        /// </summary>
        Normal,

        /// <summary>
        /// Represent the hard level.
        /// </summary>
        Hard
    }
}
