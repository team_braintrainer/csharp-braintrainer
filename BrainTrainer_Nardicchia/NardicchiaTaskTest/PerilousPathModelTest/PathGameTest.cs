﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NardicchiaTask.Model.PerilousPathModel;
using NardicchiaTask.Model;

namespace NardicchiaTaskTest.PerilousPathModelTest
{
    /// <summary>
    /// Class to test if <see cref="IPathGame">the game's path</see> works correctly.
    /// </summary>
    [TestClass]
    public class PathGameTest
    {
        private IPerilousPathDifficulty difficulty;
        private LinkedList<KeyValuePair<int, int>> path;

        /// <summary>
        /// Initializes a new instance of the <see cref="PathGameTest"/> class.
        /// </summary>
        public PathGameTest()
        {
            this.difficulty = new PerilousPathDifficultyBuilderImpl().SetDifficultyLevel(DifficultyLevel.Easy).Build();
        }

        /// <summary>
        /// Test the length game's path.
        /// </summary>
        [TestMethod]
        public void PathGameLengthTest()
        {
            this.path = PathGameImpl.CreateRandomDirectionPath(this.difficulty.GetSize(), this.difficulty.GetNumMines()).GetPathList();
            Assert.IsTrue(this.path.Count <= (this.difficulty.GetSize() * this.difficulty.GetSize()) - this.difficulty.GetNumMines());
        }

        /// <summary>
        /// Test if the first element of the path is the start.
        /// </summary>
        [TestMethod]
        public void PathGameDefaultStartTest()
        {
            this.path = PathGameImpl.CreateRandomDirectionPathDefaultStart(this.difficulty.GetSize(), this.difficulty.GetNumMines()).GetPathList();
            Assert.AreEqual(new KeyValuePair<int, int>(0, 0), this.path.First.Value);
        }

        /// <summary>
        /// Test exception if the size and the number of mines are equals to zero.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "The size and the number of mines can't be equals to zero.")]
        public void PathGameZeroTest()
        {
            PathGameImpl.CreateRandomDirectionPath(0, 0).GetPathList();
        }
    }
}
