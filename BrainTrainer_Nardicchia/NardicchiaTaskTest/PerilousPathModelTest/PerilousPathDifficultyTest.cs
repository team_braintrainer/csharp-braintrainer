﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NardicchiaTask.Model.PerilousPathModel;
using NardicchiaTask.Model;

namespace NardicchiaTaskTest.PerilousPathModelTest
{
    /// <summary>
    /// Class to test if <see cref="IPerilousPathDifficulty">the perilous path difficulty</see> works correctly.
    /// </summary>
    [TestClass]
    public class PerilousPathDifficultyTest
    {
        private IPerilousPathDifficulty difficulty;
        private IPerilousPathDifficultyBuilder db;

        /// <summary>
        /// Initialize the perilous path difficulty builder at each test.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.db = new PerilousPathDifficultyBuilderImpl();
        }

        /// <summary>
        /// Test the default easy difficulty.
        /// </summary>
        [TestMethod]
        public void EasyDefaultDifficultyTest()
        {
            this.difficulty = this.InitDifficulty(DifficultyLevel.Easy);
            Assert.AreEqual(5, this.difficulty.GetSize());
            Assert.AreEqual(10, this.difficulty.GetNumMines());
        }

        /// <summary>
        /// Test the easy difficulty.
        /// </summary>
        [TestMethod]
        public void EasyDifficultyTest()
        {
            this.difficulty = new PerilousPathDifficultyBuilderImpl().SetDifficultyLevel(DifficultyLevel.Easy)
                                                                     .SetSize(6)
                                                                     .SetNumMines(12)
                                                                     .Build();
            Assert.AreEqual(6, this.difficulty.GetSize());
            Assert.AreEqual(12, this.difficulty.GetNumMines());
        }

        /// <summary>
        /// Test the default normal difficulty.
        /// </summary>
        [TestMethod]
        public void NormalDefaultDifficultyTest()
        {
            this.difficulty = this.InitDifficulty(DifficultyLevel.Normal);
            Assert.AreEqual(6, this.difficulty.GetSize());
            Assert.AreEqual(20, this.difficulty.GetNumMines());
        }

        /// <summary>
        /// Test the default hard difficulty.
        /// </summary>
        [TestMethod]
        public void HardDefaultDifficultyTest()
        {
            this.difficulty = this.InitDifficulty(DifficultyLevel.Hard);
            Assert.AreEqual(7, this.difficulty.GetSize());
            Assert.AreEqual(30, this.difficulty.GetNumMines());
        }

        /// <summary>
        /// Test exception if the current difficulty level has been omitted.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "The current difficulty level can't be omitted.")]
        public void DifficultyLevelNoSpecifiedTest()
        {
            new PerilousPathDifficultyBuilderImpl().Build();
        }

        /// <summary>
        /// Test exception if the same object is used plus of one time.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "The same object can't be used plus of one time.")]
        public void UseExceptionDifficultyTest()
        {
            this.difficulty = this.InitDifficulty(DifficultyLevel.Easy);
            this.db.SetDifficultyLevel(DifficultyLevel.Easy).SetSize(6).Build();
            this.db.SetDifficultyLevel(DifficultyLevel.Easy).Build();
        }

        /// <summary>
        /// Test exception if the default value size modified is invalid.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "The default value size modified is invalid.")]
        public void InvalidSizeExceptionTest()
        {
            this.db.SetDifficultyLevel(DifficultyLevel.Easy).SetSize(3).Build();
        }

        /// <summary>
        /// Test exception if the default value numMines modified is invalid.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "The default value numMines modified is invalid.")]
        public void InvalidNumMinesExceptionTest()
        {
            this.db.SetDifficultyLevel(DifficultyLevel.Easy).SetNumMines(25).Build();
        }

        /// <summary>
        /// Test exception if the size value has been added two times.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "The size value can't be added two times.")]
        public void AddSizeTwoTimesExceptionTest()
        {
            this.db.SetDifficultyLevel(DifficultyLevel.Easy).SetSize(6).SetSize(7).Build();
        }

        /// <summary>
        /// Test exception if the numMines value has been added two times.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "The numMines value can't be added two times.")]
        public void AddNumMinesTwoTimesExceptionTest()
        {
            this.db.SetDifficultyLevel(DifficultyLevel.Easy).SetNumMines(13).SetNumMines(15).Build();
        }

        private IPerilousPathDifficulty InitDifficulty(DifficultyLevel difficulty)
        {
            return this.db.SetDifficultyLevel(difficulty).Build();
        }
    }
}
